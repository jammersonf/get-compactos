<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('background')->nullable();
            $table->string('logo')->nullable();
            $table->string('head_title')->nullable();
            $table->string('head_subtitle')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->string('status');
            $table->text('txt');
            $table->string('img1');
            $table->string('img2')->nullable();
            $table->text('column1');
            $table->text('column2');
            $table->string('icon1')->nullable();
            $table->string('txt1')->nullable();
            $table->string('icon2')->nullable();
            $table->string('txt2')->nullable();
            $table->string('icon3')->nullable();
            $table->string('txt3')->nullable();
            $table->string('icon4')->nullable();
            $table->string('txt4')->nullable();
            $table->string('icon5')->nullable();
            $table->string('txt5')->nullable();
            $table->string('icon6')->nullable();
            $table->string('txt6')->nullable();
            $table->string('icon7')->nullable();
            $table->string('txt7')->nullable();
            $table->string('icon8')->nullable();
            $table->string('txt8')->nullable();
            $table->string('icon9')->nullable();
            $table->string('txt9')->nullable();
            $table->string('icon10')->nullable();
            $table->string('txt10')->nullable();
            $table->string('icon11')->nullable();
            $table->string('txt11')->nullable();
            $table->string('icon12')->nullable();
            $table->string('txt12')->nullable();
            $table->string('icon13')->nullable();
            $table->string('txt13')->nullable();
            $table->string('icon14')->nullable();
            $table->string('txt14')->nullable();
            $table->string('icon15')->nullable();
            $table->string('txt15')->nullable();
            $table->string('book');
            $table->string('imgbook');
            $table->string('andtxt1')->default('Item 1');
            $table->integer('andpct1')->default(0);
            $table->string('andtxt2')->default('Item 2');
            $table->integer('andpct2')->default(0);
            $table->string('andtxt3')->default('Item 3');
            $table->integer('andpct3')->default(0);
            $table->string('andtxt4')->default('Item 4');
            $table->integer('andpct4')->default(0);
            $table->string('andtxt5')->default('Item 5');
            $table->integer('andpct5')->default(0);
            $table->string('andtxt6')->default('Item 6');
            $table->integer('andpct6')->default(0);
            $table->string('andtxt7')->default('Projeto Total');
            $table->integer('andpct7')->default(0);
            $table->date('andent');
            $table->date('andpr');
            $table->string('lat');
            $table->string('long');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp');
    }
}
