<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('emp_id');
            $table->string('1tt')->nullable();
            $table->string('1stt')->nullable();
            $table->string('2tt')->nullable();
            $table->string('2stt')->nullable();
            $table->string('3tt')->nullable();
            $table->string('3stt')->nullable();
            $table->string('4tt')->nullable();
            $table->string('4stt')->nullable();
            $table->string('5tt')->nullable();
            $table->string('5stt')->nullable();
            $table->string('6tt')->nullable();
            $table->string('6stt')->nullable();
            $table->string('type');
            $table->string('img');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_photos');
    }
}
