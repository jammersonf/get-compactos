<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEmpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp', function (Blueprint $table) {
            $table->string('ch_img')->nullable();
            $table->string('ch_title')->nullable();
            $table->string('ch_subtitle')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp', function (Blueprint $table) {
            $table->dropColumn('ch_img');
            $table->dropColumn('ch_title');
            $table->dropColumn('ch_subtitle');
        });
    }
}
