<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'laravel-filemanager'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
Route::get('robots.txt', 'RobotsController');
Route::get('sitemap', 'HomeController@sitemap');

Route::name('home')->get('/', 'HomeController@index');
Route::name('404')->get('/404', 'HomeController@notFound');
Route::name('empreendimentos')->get('empreendimentos', 'EmpController@index');
Route::name('emp')->get('empreendimentos/{slug}', 'EmpController@slug');
Route::name('emp.download')->get('empreendimentos/{slug}/download', 'EmpController@download');
Route::name('emp.contact')->post('empreendimentos/{slug}/contato', 'EmpController@contact');
Route::name('about')->get('/sobre', 'AboutController@index');
Route::name('builder')->get('/monteoseu', 'BuilderController@index');
Route::name('contact')->get('/contato', 'ContactController@index');
Route::name('contact.send')->post('/contato', 'ContactController@store');

Route::name('conteudo')->get('conteudos/{slug}', 'ConteudosController@slug');
Route::name('conteudos')->get('conteudos', 'ConteudosController@index');

Auth::routes(['register' => false]);

Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/', 'AdminController@index')->name('panel');

    Route::prefix('home')->name('home.')->group(function () {
        Route::get('create', 'AdminController@create')->name('create');
        Route::post('store', 'AdminController@store')->name('store');
        Route::post('{id}/destroy', 'AdminController@destroy')->name('destroy');
        Route::get('{id}', 'AdminController@show')->name('show');
        Route::post('{id}', 'AdminController@update')->name('update');
        Route::get('/', 'AdminController@home')->name('index');
    });
    Route::prefix('destaques')->name('feat.')->group(function () {
        Route::get('create', 'FeatureController@create')->name('create');
        Route::post('store', 'FeatureController@store')->name('store');
        Route::post('{id}/destroy', 'FeatureController@destroy')->name('destroy');
        Route::get('{id}', 'FeatureController@show')->name('show');
        Route::post('{id}', 'FeatureController@update')->name('update');
        Route::get('/', 'FeatureController@index')->name('index');
    });
    Route::prefix('empreendimentos')->name('emp.')->group(function () {
        Route::get('create', 'EmpController@create')->name('create');
        Route::post('store', 'EmpController@store')->name('store');
        Route::post('{id}/destroy', 'EmpController@destroy')->name('destroy');
        Route::get('{id}', 'EmpController@show')->name('show');
        Route::post('{id}', 'EmpController@update')->name('update');
        Route::get('/', 'EmpController@admin')->name('admin');

        Route::get('{emp}/fotos/create', 'EmpPhotoController@create')->name('photo.create');
        Route::post('{emp}/fotos/store', 'EmpPhotoController@store')->name('photo.store');
        Route::post('{emp}/fotos/{id}/destroy', 'EmpPhotoController@destroy')->name('photo.destroy');
        Route::get('{emp}/fotos/{id}', 'EmpPhotoController@show')->name('photo.show');
        Route::post('{emp}/fotos/{id}', 'EmpPhotoController@update')->name('photo.update');
        Route::get('{emp}/fotos', 'EmpPhotoController@index')->name('photo.index');

        Route::get('{emp}/pavimentos/create', 'EmpFloorController@create')->name('floor.create');
        Route::post('{emp}/pavimentos/store', 'EmpFloorController@store')->name('floor.store');
        Route::post('{emp}/pavimentos/{id}/destroy', 'EmpFloorController@destroy')->name('floor.destroy');
        Route::get('{emp}/pavimentos/{id}', 'EmpFloorController@show')->name('floor.show');
        Route::post('{emp}/pavimentos/{id}', 'EmpFloorController@update')->name('floor.update');
        Route::get('{emp}/pavimentos', 'EmpFloorController@index')->name('floor.index');

        Route::get('{emp}/pagamentos/create', 'EmpPaymentController@create')->name('payment.create');
        Route::post('{emp}/pagamentos/store', 'EmpPaymentController@store')->name('payment.store');
        Route::post('{emp}/pagamentos/{id}/destroy', 'EmpPaymentController@destroy')->name('payment.destroy');
        Route::get('{emp}/pagamentos/{id}', 'EmpPaymentController@show')->name('payment.show');
        Route::post('{emp}/pagamentos/{id}', 'EmpPaymentController@update')->name('payment.update');
        Route::get('{emp}/pagamentos', 'EmpPaymentController@index')->name('payment.index');
    });

    Route::prefix('pins')->name('pin.')->group(function () {
        Route::get('create', 'PinController@create')->name('create');
        Route::post('store', 'PinController@store')->name('store');
        Route::post('{id}/destroy', 'PinController@destroy')->name('destroy');
        Route::get('{id}', 'PinController@show')->name('show');
        Route::post('{id}', 'PinController@update')->name('update');
        Route::get('/', 'PinController@index')->name('index');

        Route::get('{pin}/ad/create', 'PinAdController@create')->name('ad.create');
        Route::post('{pin}/ad/store', 'PinAdController@store')->name('ad.store');
        Route::post('{pin}/ad/{id}/destroy', 'PinAdController@destroy')->name('ad.destroy');
        Route::get('{pin}/ad/{id}', 'PinAdController@show')->name('ad.show');
        Route::post('{pin}/ad/{id}', 'PinAdController@update')->name('ad.update');
        Route::get('{pin}/ad', 'PinAdController@index')->name('ad.index');
    });

    Route::prefix('pavimentos')->name('unity.')->group(function () {
        Route::get('{floor}/unidades/create', 'EmpFloorUnityController@create')->name('create');
        Route::post('{floor}/unidades/store', 'EmpFloorUnityController@store')->name('store');
        Route::post('{floor}/unidades/{id}/destroy', 'EmpFloorUnityController@destroy')->name('destroy');
        Route::get('{floor}/unidades/{id}', 'EmpFloorUnityController@show')->name('show');
        Route::post('{floor}/unidades/{id}', 'EmpFloorUnityController@update')->name('update');
        Route::get('{floor}/unidades', 'EmpFloorUnityController@index')->name('index');
    });

    Route::prefix('unidades')->name('planned.')->group(function () {
        Route::get('{p}/planejados/create', 'EmpFloorUnityPlannedController@create')->name('create');
        Route::post('{p}/planejados/store', 'EmpFloorUnityPlannedController@store')->name('store');
        Route::post('{p}/planejados/{id}/destroy', 'EmpFloorUnityPlannedController@destroy')->name('destroy');
        Route::get('{p}/planejados/{id}', 'EmpFloorUnityPlannedController@show')->name('show');
        Route::post('{p}/planejados/{id}', 'EmpFloorUnityPlannedController@update')->name('update');
        Route::get('{p}/planejados', 'EmpFloorUnityPlannedController@index')->name('index');
    });

    Route::prefix('blog')->name('blog.')->group(function () {
        Route::get('create', 'BlogController@create')->name('create');
        Route::post('store', 'BlogController@store')->name('store');
        Route::post('{id}/destroy', 'BlogController@destroy')->name('destroy');
        Route::get('{id}', 'BlogController@show')->name('show');
        Route::post('{id}', 'BlogController@update')->name('update');
        Route::get('/', 'BlogController@index')->name('index');
    });

    Route::prefix('contatos')->name('contact.')->group(function () {
        Route::get('create', 'ContactController@create')->name('create');
        Route::post('store', 'ContactController@store')->name('store');
        Route::post('{id}/destroy', 'ContactController@destroy')->name('destroy');
        Route::get('{id}/simulation', 'ContactController@simulation')->name('simulation');
        Route::get('{id}', 'ContactController@show')->name('show');
        Route::post('{id}', 'ContactController@update')->name('update');
        Route::get('/', 'ContactController@admin')->name('admin');
    });

    Route::prefix('eletros')->name('electro.')->group(function () {
        Route::get('create', 'ElectroController@create')->name('create');
        Route::post('store', 'ElectroController@store')->name('store');
        Route::post('{id}/destroy', 'ElectroController@destroy')->name('destroy');
        Route::get('{id}', 'ElectroController@show')->name('show');
        Route::post('{id}', 'ElectroController@update')->name('update');
        Route::get('/', 'ElectroController@index')->name('index');
    });

    Route::prefix('sobre')->name('about.')->group(function () {
        Route::get('create', 'AboutController@create')->name('create');
        Route::post('store', 'AboutController@store')->name('store');
        Route::post('{id}/destroy', 'AboutController@destroy')->name('destroy');
        Route::get('{id}', 'AboutController@show')->name('show');
        Route::post('{id}', 'AboutController@update')->name('update');
        Route::get('/', 'AboutController@admin')->name('admin');
    });
});

