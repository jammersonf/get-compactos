@extends('components.layouts.default')


@section('metas')
    <title>Contato - Get Compactos</title>
    <meta name="description"
    content="Construtora e incorporadora.  Inovação, tecnologia, sustentabilidade e modernidade em forma de imóveis  compactos. Sua liberdade começa aqui. Let’s Get! ">
    <meta name="keywords"
    content="get, get compactos, construtora,  incorporadora, imóveis, loft, compacto, tecnologia, apartamento, alto  padrão, empreendimento imobiliário, joão pessoa, arquitetura, mercado  imobiliário">
    <meta name="robots" content="">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="Portuguese">
    <meta name="generator" content="N/A">
    <meta http-equiv="Content-Type" content="text/html;  charset=utf-8">
@endsection

@section('content')
<div class="background-cinza">


    @include('components.partials.navbar_light')
    @include('components.partials.contact.contact')

    @include('components.partials.contact.mapa')
    @include('components.partials.footer')
</div>
@endsection