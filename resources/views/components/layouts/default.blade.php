<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    @yield('metas')
    
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <link rel="shortcut icon" type="image/png" href="{{asset('assets_front/imgs/favicon.png')}}?ver=30.0" />

    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule="" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.compat.css">
    <link href="{{asset('assets_front/icons/fontawesome/css/all.css')}}?ver=30.0" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="{{asset('assets_front/css/bootstrap.css')}}?ver=30.0">
    <link rel="stylesheet" href="{{asset('assets_front/icon-fonts/css/style.css')}}?ver=30.0">
    <link rel="stylesheet" href="{{asset('assets_front/css/style.css')}}?ver=30.0">

    @toastr_css
    {!! SEO::generate() !!}
    {!! Robots::metaTag() !!}
    @include('components.partials.analytcs')
</head>

<body>
    @yield('content')
    @stack('modals')
    @jquery

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <!-- Bootstrap JS -->
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
    integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous">
    </script> --}}
    <script src="{{asset('assets_front/js/bootstrap.js')}}"></script>
    {{-- <script src="{{asset('assets_front/js/image-map.jquery.js')}}"></script> --}}
    <script src="{{asset('assets_front/js/imageMapResizer.min.js')}}"></script>
    {{-- <script src="{{asset('assets_front/js/jquery.imagemapster.min.js')}}"></script> --}}
    {{-- <script src="{{asset('assets_front/js/jquery.maphilight.min.js')}}"></script> --}}

    

    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
    @toastr_js
    @toastr_render
    {{-- <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script> --}}
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init({
            offset: 120, // offset (in px) from the original trigger point
            duration: 1000, // values from 0 to 3000, with step 50ms
            easing: 'easeOutBack', // default easing for AOS animations
            mirror: false, // whether elements should animate out while scrolling past them
            once: true
        });
    </script>
    @stack('scripts')
    <script>
        $('.carousel').each(function (value) {  
            $(this).carousel({
                touch: true
            })
        })
    </script>
    <script>
        /* $('.img-map-styles').maphilight(); */
        /* $('.img-map-styles').mouseover(function(e) {
            $('#squidhead').mouseover();
        }).mouseout(function(e) {
            $('#squidhead').mouseout();
        }).click(function(e) { e.preventDefault(); }); */
        //$('img').mapster();
        
    </script>
</body>

</html>
