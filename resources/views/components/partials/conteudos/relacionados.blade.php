<div class="container pt-30 text-center">
    <h2 data-aos="fade-right" style="max-width: 747px" class="text-preto-azulado-get mr-auto ml-auto">Artigos relacionados</h2>
    <p data-aos="fade-right" data-aos-delay="200" style="max-width: 660px" class="paragraph-20 text-dark mr-auto ml-auto mb-0">Continue lendo e mantenha-se atualizado com nossas novidades</p>
    <div style="margin-bottom: 90px" class="row blog-destaques ">
            @foreach($blog as $b)
            @if($b->id !== $data->id)
            <div data-aos="flip-right" class="col-md-4 ">
                <div class="card card-transparent card-width mx-auto"> {{--  --}}
                    <img onclick="location.href= '{{ route('conteudo', $b->slug)}}';" src="{{url('storage/blog/'.$b->img)}}" class="card-img-top img-cover click-link" alt="...">
                    <div class="card-body px-0 text-center">
                        <span class="span-16">{{$b->date}}</span>
                        <a href="{{ route('conteudo', $b->slug)}}">
                            <h4 class="text-preto-azulado-get">
                                {!! \Illuminate\Support\Str::limit($b->title, 40, '...')!!}
                            </h4>
                        </a>
                        <a href="{{ route('conteudo', $b->slug)}}" class="btn btn-outline-dark blog-destaques-btn-spacing btn-smaller">Ler mais</a>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
    </div>

</div>
