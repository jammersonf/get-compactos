<footer>
    <div class="container d-print-none">
        <div class="row text-center footer-spacing">
            <div class="col-md-4">
                <img style="margin-top: 38px" src="{{ asset('assets_front/imgs/Logo Branca.png') }}" width="102px" alt="">
            </div>
            <div class="col-md-4">
                <a class="d-block footer-menu-link  text-white fw-600 caption-20" href="{{ route('about') }}">Sobre a Get</a> 
                <a class="d-block footer-menu-link  text-white fw-600 caption-20" href="{{ route('empreendimentos') }}">Empreendimentos </a> 
                <a class="d-block footer-menu-link  text-white fw-600 caption-20" href="{{ route('conteudos') }}">Conteúdos</a> 
                <a class="d-block footer-menu-link  text-white fw-600 caption-20" href="{{ route('contact') }}">Contato</a>
            </div>
            <div class="col-md-4">
                <div class="d-flex justify-content-center">
                    <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://wa.me/5583996361415?text=Olá%20tenho%20interesse%20nos%20empreendimentos%20da%20get%20compactos."><ion-icon style="font-size: 24px;" name="logo-whatsapp"></ion-icon></a>
                    <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://www.instagram.com/getcompactos/"><ion-icon style="font-size: 24px;" name="logo-instagram"></ion-icon></a>
                </div>
                <p class="text-white caption-20"style="font-weight: 500">Fale Conosco</p>
                <a href="tel:83-3031-9191">
                    <p class="text-white caption-20"  >+55 083 3031-9191</p>
                </a>
            </div>
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-center">
        <p style="font-weight: 500" class="caption-16 text-white  mr-3">© Copyright 2020 Get Compactos.</p>
        <a target="_blank" href="https://www.qualitare.com/" class="footer-by footer-qualitare">
            <img src="{{ asset('assets_front/imgs/qualitare.png') }}"  alt="">
        </a>
    </div>
</footer>