<div class="row">{{-- unities --}}
    <div class="col-md-5 text-left mx-2 mx-md-0">
        <div class="ml-0 ml-md-4 mt-32">
            <p class="animated fadeInLeft caption-14 text-hex-797979">
                Passo 4 de 8
            </p>
            <h4 class="animated fadeInLeft delay-200ms text-preto-azulado-get">
                Escolha o apartamento
            </h4>
            <p style="max-width: 360px;" class="animated fadeInLeft delay-400ms caption-15 text-preto-azulado-get mb-0 mb-md-5 pb-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu sagittis.
            </p>

            <div onclick="openSelectStepDataMobile('#step-4-mobile-data')" class="animated fadeInLeft delay-400ms select-step-mobile d-flex d-md-none mb-4 justify-content-between">
                <div class="d-flex justify-content-start">
                    <p id="step-4-mobile-data-label" class="caption-15 font-weight-bold text-hex-797979 mb-0">Selecione aqui o apartamento</p>
                    <span id="step-4-mobile-data-value" class="caption-15 font-weight-normal text-hex-797979 mb-0 float-left ml-2"></span>
                </div>
                <ion-icon name="chevron-down-outline"></ion-icon>
            </div>

            <div id="ap-select-area" class="animated fadeInLeft delay-600ms d-none d-md-block">
            </div>

        </div>
    </div>
    <div data-aos="fade" class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_ap_detail')" id="current-ap-select-area"
            class="item-detail-img h-mob-64 mr-0 mr-md-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-ap.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-total-title">Valor parcial</p>
            <h4 class="ml-32 text-middle-gray  value-partial-class font-weight-bold"></h4>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
</div>

@push('modals')
<div class="modal fade p-0" id="modal_ap_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal fm-monte-o-seu" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button style="color: #fff" type="button" class="close btn  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon size="large" name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <img class="modal-item-img" id="modal_ap_detail-img" src="" alt="">

            </div>
        </div>
    </div>
</div>
<div class="modal fade pr-0" id="step-4-mobile-data" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog fullscreen-modal-white">
      <div class="modal-content">
        <div style="border-bottom: 0" class="modal-header">
          <button type="button" class="close mr-auto ml-0" data-dismiss="modal" aria-label="Close">
            <ion-icon size="large" name="chevron-down-outline"></ion-icon>
          </button>
        </div>
        <div class="modal-body">
            <div id="ap-select-area-mob" class="">
            </div>
        </div>
      </div>
    </div>
  </div>

@endpush
@push('scripts')
    <script>
        function selectAp(ap, floor, unity, obj) {
            
            var mobileValueFormatted = +ap.price;
            $("#step-4-mobile-data-value").html(mobileValueFormatted.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));

            $('#disabled-ap-selec').removeAttr("hidden");
            if(ap.sold == 1 && oportunity.is_client){
                setParcialValue("4", 0);
                setParcialValue("3", 0);
                setParcialValue("2", 0);
                setParcialValue("1", 0);
            }else if(ap.sold == 1) {
                return false;
            }else{
                setParcialValue("4", ap.price);
            }
            SetSelectedStep("4", obj);

            console.log("selected-ap to set unity");
            console.log(ap);
            console.log(unity);
            setItemMOSBlack("#current-ap-select-area", "#modal_ap_detail-img", `{{url('storage/unity/${ap.img}')}}`);
            if(ap.sold == 1){
                updateValueUnity(0);
            }else {
                updateValueUnity(ap.price);
            }

            setUnity(`{{url('storage/unity/${ap.img}')}}`, ap.planned, floor, unity, `uni-label-${floor.id}-${unity.id}`);

            $("#step-4-mobile-data").modal('hide')
            $("#step-4-mobile-data-label").html(ap.name);
            $("#step-4-mobile-data-label").removeClass("text-hex-797979");
            $("#step-4-mobile-data-label").addClass("text-middle-gray");

        }
    </script>
@endpush