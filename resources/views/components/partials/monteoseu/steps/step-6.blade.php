@foreach($data as $key => $emp)
@foreach($emp['floors'] as $key => $flor)
@foreach($flor['unities'] as $florkey => $uni)
@foreach($uni as $unikey => $singleUni )
{{-- <div id="collapse-electros-{{$flor['id']}}-{{$singleUni['id']}}" class="d-none">
    <div class="text-center mx-4 mx-md-0">
        <img src="{{url('assets_front/imgs/television.svg')}}" class="pb-2" alt="">
        <h4  class="text-preto-azulado-get">
            Lista de eletrodomésticos
        </h4>
        @if($singleUni['electros_new'] != null)
        <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
            Selecione o projeto de móveis planejados e siga para a próxima etapa 
        </p>
        @else
        <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
            Essa unidade não possui Eletrodomésticos cadastrados 
        </p>

        @endif
    </div>
    <div class="container-small mx-auto">
        <div class="row mb-40">
            @if($singleUni['electros_new'] != null)
            @foreach ($singleUni['electros_new'] as $su)
                <div class="col-md-6 col-right mb-20">
                    <div style="width: 115px" class="custom-control custom-checkbox get-checkbox">
                        <input onclick="setMovel({{json_decode($su)->price}}, '{{json_decode($su)->name}}', this)"style="box-shadow: none" type="checkbox" class="custom-control-input" aria-describedby="movel-value-{{json_decode($su)->id}}" id="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                        <label class="custom-control-label text-preto-azulado-get" for="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">{{json_decode($su)->name}}</label>
                        <small id="movel-value-{{json_decode($su)->id}}" class="form-text text-vermelho-get">
                            {{'R$ '.number_format(json_decode($su)->price, 2, ',', '.')}}
                        </small>
                    </div>
                </div>
            @endforeach
            @endif
        </div>
    </div>
</div> --}}


<div id="collapse-electros-{{$flor['id']}}-{{$singleUni['id']}}" class="row d-none">
    <div class="col-md-5 text-left mx-2 mx-md-0">
        <div class=" ml-0 ml-md-4 mt-32">
            <p class="animated fadeInLeft  caption-14 text-hex-797979">
                Passo 6 de 8
            </p>
            <h4 class="animated fadeInLeft delay-200ms text-preto-azulado-get">
                Que tal uns eletrodomésticos para seu apartamento?

            </h4>
            <p style="max-width: 360px;" class="animated fadeInLeft delay-400ms caption-15 text-preto-azulado-get mb-0 mb-md-5 pb-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu sagittis.
            </p>

            <div onclick="openSelectStepDataMobile('#step-6-mobile-data-{{$flor['id']}}-{{$singleUni['id']}}')" class="animated fadeInLeft delay-400ms select-step-mobile d-flex d-md-none mb-4 justify-content-between">
                <div class="d-flex justify-content-start">
                    <p id="step-6-mobile-data-label-{{$flor['id']}}-{{$singleUni['id']}}" class="caption-15 font-weight-bold text-hex-797979 mb-0">Selecione aqui o(s) eletrodoméstico(s)</p>
                    <span id="step-6-mobile-data-value-{{$flor['id']}}-{{$singleUni['id']}}" class="caption-15 font-weight-normal text-hex-797979 mb-0 float-left ml-2"></span>
                </div>
                <ion-icon name="chevron-down-outline"></ion-icon>
            </div>

            @if($singleUni['electros_new'] != null)
            @foreach ($singleUni['electros_new'] as $su){{-- {{json_decode($su)->name}} --}}
                <div class="animated fadeInLeft delay-600ms mb-20 d-none d-md-block">
                    <div class="custom-control custom-checkbox get-checkbox">
                        <input onclick="setMovel({{json_decode($su)->price}}, '{{json_decode($su)->name}}', this)"style="box-shadow: none" type="checkbox" class="custom-control-input" aria-describedby="movel-value-{{json_decode($su)->id}}" id="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                        <label class="d-flex custom-control-label text-preto-azulado-get" for="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                            {{json_decode($su)->name}}
                            <small id="movel-value-{{json_decode($su)->id}}" class="form-text text-vermelho-get ml-2">
                                + {{'R$ '.number_format(json_decode($su)->price, 2, ',', '.')}}
                            </small>
                        </label>
                    </div>
                </div>
            @endforeach
            @endif
    
        </div>
    </div>
    <div data-aos="fade" class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_electros_detail')" id="current-electros-select-area"
            class="item-detail-img h-mob-64 mr-0 mr-md-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-elec.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-total-title">Valor parcial</p>
            <h4 class="ml-32 text-middle-gray  value-total-class font-weight-bold"></h4>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
</div>
@push('modals')
<div class="modal fade pr-0" id="step-6-mobile-data-{{$flor['id']}}-{{$singleUni['id']}}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog fullscreen-modal-white">
        <div class="modal-content">
            <div style="border-bottom: 0" class="modal-header">
                <button type="button" class="close mr-auto ml-0" data-dismiss="modal" aria-label="Close">
                    <ion-icon size="large" name="chevron-down-outline"></ion-icon>
                </button>
            </div>
            <div class="modal-body">
                @if($singleUni['electros_new'] != null)
                @foreach ($singleUni['electros_new'] as $su){{-- {{json_decode($su)->name}} --}}
                    <div class="mb-20">
                        <div class="custom-control custom-checkbox get-checkbox"   >
                            <input data-label="#step-6-mobile-data-label-{{$flor['id']}}-{{$singleUni['id']}}" data-labelmob="#step-6-mobile-data-value-{{$flor['id']}}-{{$singleUni['id']}}"onclick="setMovel({{json_decode($su)->price}}, '{{json_decode($su)->name}}', this)"style="box-shadow: none" type="checkbox" class="custom-control-input" aria-describedby="movel-value-{{json_decode($su)->id}}" id="forniture-select-check-mob{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                            <label class="d-flex custom-control-label text-preto-azulado-get" for="forniture-select-check-mob{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                                {{json_decode($su)->name}}
                                <small id="movel-value-{{json_decode($su)->id}}" class="form-text text-vermelho-get ml-2">
                                    + {{'R$ '.number_format(json_decode($su)->price, 2, ',', '.')}}
                                </small>
                            </label>
                        </div>
                    </div>
                @endforeach
                @endif    
            </div>
        </div>
    </div>
</div>
@endpush



@endforeach
@endforeach
@endforeach
@endforeach

