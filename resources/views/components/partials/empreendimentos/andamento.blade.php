@if(!empty($data->andpct1) || 
    !empty($data->andpct2) ||
    !empty($data->andpct3) ||
    !empty($data->andpct4) ||
    !empty($data->andpct5) ||
    !empty($data->andpct6) ||
    !empty($data->andpct7) ||
    ($data->andent != "0000-00-00") ||
    ($data->andent != "0000-00-00"))
<div class="container-small mr-auto ml-auto my-42">
    <div class="d-flex justify-content-center header-text">
        <p data-aos="fade-right"  class="text-uppercase"><span>detalhes do</span> andamento</p>
    </div>
    <div class="row mx-0">
        @if(!empty($data->andpct1))
        <div data-aos="fade-up" data-aos-delay="200" class="col-md-4 progress-get">
            <p class="caption-18-600 mb-2 text-dark text-uppercase">{{$data->andtxt1}}</p>
            <div style="margin-right: 15px " class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{$data->andpct1}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="value"><small>{{$data->andpct1}}%</small></p>
        </div>
        @endif
        @if(!empty($data->andpct2))

        <div data-aos="fade-up" data-aos-delay="400" class="col-md-4 progress-get">
            <p class="caption-18-600 mb-2 text-dark text-uppercase">{{$data->andtxt2}}</p>
            <div style="margin-right: 15px " class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{$data->andpct2}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="value"><small>{{$data->andpct2}}%</small></p>
        </div>
        @endif
        @if(!empty($data->andpct3))
        <div data-aos="fade-up" data-aos-delay="600" class="col-md-4 progress-get">
            <p class="caption-18-600 mb-2 text-dark text-uppercase">{{$data->andtxt3}}</p>
            <div style="margin-right: 15px " class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{$data->andpct3}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="value"><small>{{$data->andpct3}}%</small></p>
        </div>
        @endif

        @if(!empty($data->andpct4))
        <div data-aos="fade-up" data-aos-delay="800" class="col-md-4 progress-get">
            <p class="caption-18-600 mb-2 text-dark text-uppercase">{{$data->andtxt4}}</p>
            <div style="margin-right: 15px " class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{$data->andpct4}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="value"><small>{{$data->andpct4}}%</small></p>
        </div>
        @endif

        @if(!empty($data->andpct5))
        <div data-aos="fade-up" data-aos-delay="1000" class="col-md-4 progress-get">
            <p class="caption-18-600 mb-2 text-dark text-uppercase">{{$data->andtxt5}}</p>
            <div style="margin-right: 15px " class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{$data->andpct5}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="value"><small>{{$data->andpct5}}%</small></p>
        </div>
        @endif

        @if(!empty($data->andpct6))
        <div data-aos="fade-up" data-aos-delay="1200" class="col-md-4 progress-get">
            <p class="caption-18-600 mb-2 text-dark text-uppercase">{{$data->andtxt6}}</p>
            <div style="margin-right: 15px " class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{$data->andpct6}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="value"><small>{{$data->andpct6}}%</small></p>
        </div>
        @endif

        @if(!empty($data->andpct7))
        <div data-aos="fade-up" data-aos-delay="1400" class="col-md-12 progress-get">
            <p class="caption-18-600 mb-2 text-dark text-uppercase">{{$data->andtxt7}}</p>
            <div style="margin-right: 15px " class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{$data->andpct7}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p class="value"><small>{{$data->andpct7}}%</small></p>
        </div>
        @endif
    </div>
    
    <div data-aos="fade-up" data-aos-delay="1600" style="margin-top: 13px; padding-left: 36px;" class="row mx-0 d-none d-md-flex">
        @php
            $month = \Carbon\Carbon::createFromFormat('Y-m-d', $data->andent);
            $dt = strtoupper($month->locale('pt_BR')->monthName);
        @endphp

        @if(($data->andent != "0000-00-00"))
        <div class="col-md-6 pr-7">
            <button class="btn btn-block btn-outline-primary-fade text-uppercase mb-2"><strong>Entrega prevista</strong>: {{$dt}} de <strong>{{date('Y', strtotime($data->andent))}}</strong></button>
        </div>
        @endif
        @php
            $month = \Carbon\Carbon::createFromFormat('Y-m-d', $data->andpr);
            $dt = strtoupper($month->locale('pt_BR')->monthName);
        @endphp
        @if(($data->andpr != "0000-00-00"))
        <div class="col-md-6 pl-10">
            <button class="btn btn-block btn-outline-primary-fade text-uppercase mb-2"><strong>prazo de entrega</strong>: {{$dt}} de <strong>{{date('Y', strtotime($data->andpr))}}</strong></button>
        </div>
        @endif
    </div>

    <div data-aos="fade-up" data-aos-delay="1600" style="margin-top: 13px;" class="row mx-0 d-md-none">
        @php
            $month = \Carbon\Carbon::createFromFormat('Y-m-d', $data->andent);
            $dt = strtoupper($month->locale('pt_BR')->monthName);
        @endphp
        @if(($data->andent != "0000-00-00"))
        <div class="col-md-6">
            <button class="btn btn-block btn-outline-primary-fade text-uppercase mb-2"><strong>Entrega prevista</strong>: <br> {{$dt}} de <strong>{{date('Y', strtotime($data->andent))}}</strong></button>
        </div>
        @endif
        @php
            $month = \Carbon\Carbon::createFromFormat('Y-m-d', $data->andpr);
            $dt = strtoupper($month->locale('pt_BR')->monthName);
        @endphp
        @if(($data->andpr != "0000-00-00"))
        <div class="col-md-6">
            <button class="btn btn-block btn-outline-primary-fade text-uppercase mb-2"><strong>prazo de entrega</strong>: {{$dt}} de <strong>{{date('Y', strtotime($data->andpr))}}</strong></button>
        </div>
        @endif
    </div>

</div>
@endif
