@if(count($data->photos) > 0)
<div data-aos="fade-up" id="fotos-sec" class="container-fluid bg-empreendimentos mt-70 mt-mob-0 px-0 overflow-hidden">
    <div class="text-center py-50 ">
        <h2 data-aos="fade-right" class="ml-auto mr-auto text-white fotos-desc-width">Fotos do Projeto</h2>
        <p data-aos="fade-up" data-aos-delay="100" class="ml-auto mr-auto text-white fotos-desc-width">
            Cada detalhe do Get One foi pensado para combinar com o seu estilo de vida.
        </p>
    </div>
    <div class="overflow-auto">

    <ul data-aos="fade-up" data-aos-delay="300"
        class="nav nav-pills justify-content-start justify-content-md-center mb-3 fotos-filter" id="pills-tab"
        role="tablist">
        <li class="nav-item">
            <a class="nav-link @if($q == 'all') active @endif" data-toggle="pill" id="pills-todos-tab"
                href="#pills-todos" role="tab" aria-controls="pills-todos" aria-selected="false">
                Todos
            </a>{{-- ?q=all --}}
        </li>
        @if(isset($photos['Interna']))
        <li class="nav-item">
            <a class="nav-link @if($q == 'interna') active @endif" data-toggle="pill" id="pills-internas-tab"
                href="#pills-internas" role="tab" aria-controls="pills-internas" aria-selected="false">
                Internas
            </a>{{-- ?q=interna --}}
        </li>
        @endif
        @if(isset($photos['Externa']))
        <li class="nav-item">
            <a class="nav-link @if($q == 'externa') active @endif" data-toggle="pill" id="pills-externas-tab"
                href="#pills-externas" role="tab" aria-controls="pills-externas" aria-selected="false">
                externas
            </a>{{-- ?q=externa --}}
        </li>
        @endif
        @if(isset($photos['Planta']))
        <li class="nav-item">
            <a class="nav-link @if($q == 'planta') active @endif" data-toggle="pill" id="pills-plantas-tab"
                href="#pills-plantas" role="tab" aria-controls="pills-plantas" aria-selected="false">
                plantas
            </a>{{-- ?q=planta --}}
        </li>
        @endif
    </ul>
</div>

    <div class="tab-content mt-40" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-todos" role="tabpanel" aria-labelledby="pills-home-tab">
            <div id="carousel_todos" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @php $key = 0; @endphp
                    @foreach($photos as $type)
                    @foreach($type as $photo)
                    @include('components.partials.component_atoms.fotos_info')
                    @php $key ++; @endphp
                    @endforeach
                    @endforeach
                </div>

                <div class="d-flex justify-content-center mt-40 pb-72">
                    <button id="fullscreenModal_todos"
                        class="btn btn-outline-primary btn-bigger text-uppercase mx-14 btn-mob-smaller">Tela
                        cheia</button>
                    <div class="indicators-counter ml-14">
                        <p style="padding: 10px 14px;" class="text-white m-0 btn-current-smaller">
                            <span class="font-weight-bold current-carousel-pos">1/</span>
                            <span class="font-weight-medium">{{count($data->photos)}}</span>
                        </p>
                    </div>
                    <ol class="carousel-indicators mt-3">
                        @php $key = 0; @endphp
                        @foreach($photos as $type)
                        @foreach($type as $photo)
                        <li data-target="#carousel_todos" id="indicators-all-{{$key}}" data-slide-to="{{$key}}" class="indicators-all @if($key == 0) active @endif @if($key > 2) d-mob-none @endif">
                        </li>
                        @php $key ++; @endphp
                        @endforeach
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
        @if(isset($photos['Interna']))
        <div class="tab-pane fade" id="pills-internas" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div id="carousel_internas" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($photos['Interna'] as $key => $photo)
                    @include('components.partials.component_atoms.fotos_info')
                    @endforeach
                </div>

                <div class="d-flex justify-content-center mt-40 pb-72">
                    <button id="fullscreenModal_internas"
                        class="btn btn-outline-primary btn-bigger text-uppercase mx-14 btn-mob-smaller">Tela
                        cheia</button>
                    <div class="indicators-counter ml-14">
                        <p style="padding: 10px 14px;" class="text-white m-0">
                            <span class="font-weight-bold current-carousel-pos">1/</span>
                            <span class="font-weight-medium">{{count($photos['Interna'])}}</span>
                        </p>
                    </div>
                    <ol class="carousel-indicators mt-3">
                        @foreach($photos['Interna'] as $key => $photo)
                        <li data-target="#carousel_internas" id="indicators-intern-{{$key}}"data-slide-to="{{$key}}"
                            class="indicators-intern @if($key == 0) active @endif @if($key > 2) d-mob-none @endif"></li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
        @endif
        @if(isset($photos['Externa']))
        <div class="tab-pane fade" id="pills-externas" role="tabpanel" aria-labelledby="pills-contact-tab">
            <div id="carousel_externas" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($photos['Externa'] as $key => $photo)
                    @include('components.partials.component_atoms.fotos_info')
                    @endforeach
                </div>

                <div class="d-flex justify-content-center mt-40 pb-72">
                    <button id="fullscreenModal_externas"
                        class="btn btn-outline-primary btn-bigger text-uppercase mx-14 btn-mob-smaller">Tela
                        cheia</button>
                    <div class="indicators-counter ml-14">
                        <p style="padding: 10px 14px;" class="text-white m-0">
                            <span class="font-weight-bold current-carousel-pos">1/</span>
                            <span class="font-weight-medium">{{count($photos['Externa'])}}</span>
                        </p>
                    </div>
                    <ol class="carousel-indicators mt-3">
                        @foreach($photos['Externa'] as $key => $photo)
                        <li data-target="#carousel_externas" id="indicators-extern-{{$key}}" data-slide-to="{{$key}}"
                            class="indicators-extern @if($key == 0) active @endif @if($key > 2) d-mob-none @endif"></li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
        @endif
        @if(isset($photos['Planta']))
        <div class="tab-pane fade" id="pills-plantas" role="tabpanel" aria-labelledby="pills-contact-tab">
            <div id="carousel_plantas" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($photos['Planta'] as $key => $photo)
                    @include('components.partials.component_atoms.fotos_info')
                    @endforeach
                </div>

                <div class="d-flex justify-content-center mt-40 pb-72">
                    <button id="fullscreenModal_plantas"
                        class="btn btn-outline-primary btn-bigger btn-mob-smaller text-uppercase mx-14">Tela
                        cheia</button>
                    <div class="indicators-counter ml-14">
                        <p style="padding: 10px 14px;" class="text-white m-0">
                            <span class="font-weight-bold current-carousel-pos">1/</span>
                            <span class="font-weight-medium">{{count($photos['Planta'])}}</span>
                        </p>
                    </div>
                    <ol class="carousel-indicators mt-3">
                        @foreach($photos['Planta'] as $key => $photo)
                        <li data-target="#carousel_plantas" id="indicators-plans-{{$key}}" data-slide-to="{{$key}}"
                            class="indicators-plans @if($key == 0) active @endif @if($key > 2) d-mob-none @endif"></li>
                        @endforeach
                    </ol>
                </div>
            </div>

        </div>
        @endif
    </div>
</div>
@endif


@push('modals')
<!-- Modal -->
@php $key = 0; @endphp
<div class="modal fade p-0" id="modal_todos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"{{-- {{$key}} --}}
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div id="carousel_todos_full" class="carousel slide carousel-fade v-center animate-css" data-ride="carousel">{{-- -{{$key}} --}}
                    <div class="carousel-inner animate-css">

                       {{--  @foreach($photos as $type)
                        @foreach($type as $i => $photo)
                        <div class="carousel-item  @if($key == 0) active @endif ">
                            <img class="d-block w-100 img-cover img-fotos" style="height: 80vh"
                                src="{{url('storage/emp/'.$photo->img)}}" alt="First slide">
                        </div>
                        @endforeach
                        @endforeach --}}

                        @if(isset($photos['Interna']))
                        @foreach($photos['Interna'] as $photo)
                        <div class="carousel-item  @if($key == 0) active @endif ">
                            <img class="d-block w-100 img-cover img-fotos" style="height: 80vh"
                            src="{{url('storage/emp/'.$photo->img)}}" alt="First slide">
                        </div>
                        @php $key ++; @endphp
                        @endforeach
                        @endif
                        @if(isset($photos['Externa']))
                        @foreach($photos['Externa'] as $photo)
                        <div class="carousel-item  @if($key == 0) active @endif ">
                            <img class="d-block w-100 img-cover img-fotos" style="height: 80vh"
                            src="{{url('storage/emp/'.$photo->img)}}" alt="First slide">
                        </div>
                        @php $key ++; @endphp
                        @endforeach
                        @endif
                        @if(isset($photos['Planta']))
                        @foreach($photos['Planta'] as $photo)
                        <div class="carousel-item animate-css  @if($key == 0) active @endif ">
                            <img class="d-block w-100 img-cover img-fotos-planta" 
                            src="{{url('storage/emp/'.$photo->img)}}" alt="First slide">
                        </div>
                        @php $key ++; @endphp
                        @endforeach
                        @endif

                    </div>
                    <a class="btn btn-light close p-2 btn-round btn-left" href="#carousel_todos_full" role="button" data-slide="prev"><ion-icon name="arrow-back-outline"></ion-icon></a>
                    <a class="btn btn-light close p-2 btn-round btn-right" href="#carousel_todos_full" role="button" data-slide="next"><ion-icon name="arrow-forward-outline"></ion-icon></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade p-0" id="modal_internas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div id="carousel_internas_full" class="carousel slide carousel-fade" data-ride="carousel">{{-- -{{$key}} --}}
                    <div class="carousel-inner">

                        @if(isset($photos['Interna']))
                        @foreach($photos['Interna'] as $key => $photo)
                        <div class="carousel-item  @if($key == 0) active @endif ">
                            <img class="d-block w-100 img-cover img-fotos" style="height: 80vh"
                            src="{{url('storage/emp/'.$photo->img)}}" alt="First slide">
                        </div>
                        @endforeach
                        @endif
                        
                    </div>
                    <a class="btn btn-light close p-2 btn-round btn-left" href="#carousel_internas_full" role="button" data-slide="prev"><ion-icon name="arrow-back-outline"></ion-icon></a>
                    <a class="btn btn-light close p-2 btn-round btn-right" href="#carousel_internas_full" role="button" data-slide="next"><ion-icon name="arrow-forward-outline"></ion-icon></a>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade p-0" id="modal_externas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div id="carousel_externas_full" class="carousel slide carousel-fade" data-ride="carousel">{{-- -{{$key}} --}}
                    <div class="carousel-inner">

                        @if(isset($photos['Externa']))
                        @foreach($photos['Externa'] as $key => $photo)
                        <div class="carousel-item  @if($key == 0) active @endif ">
                            <img class="d-block w-100 img-cover img-fotos" style="height: 80vh"
                            src="{{url('storage/emp/'.$photo->img)}}" alt="First slide">
                        </div>
                        @endforeach
                        @endif
                        
                    </div>
                    <a class="btn btn-light close p-2 btn-round btn-left" href="#carousel_externas_full" role="button" data-slide="prev"><ion-icon name="arrow-back-outline"></ion-icon></a>
                    <a class="btn btn-light close p-2 btn-round btn-right" href="#carousel_externas_full" role="button" data-slide="next"><ion-icon name="arrow-forward-outline"></ion-icon></a>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade p-0" id="modal_plantas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div id="carousel_plantas_full" class="carousel slide carousel-fade v-center" data-ride="carousel">{{-- -{{$key}} --}}
                    <div class="carousel-inner">

                        @if(isset($photos['Planta']))
                        @foreach($photos['Planta'] as $key => $photo)
                        <div class="carousel-item  @if($key == 0) active @endif ">
                            <img class="d-block w-100 img-cover img-fotos-planta" 
                            src="{{url('storage/emp/'.$photo->img)}}" alt="First slide">
                        </div>
                        @endforeach
                        @endif
                        
                    </div>
                    <a class="btn btn-light close p-2 btn-round btn-left" href="#carousel_plantas_full" role="button" data-slide="prev"><ion-icon name="arrow-back-outline"></ion-icon></a>
                    <a class="btn btn-light close p-2 btn-round btn-right" href="#carousel_plantas_full" role="button" data-slide="next"><ion-icon name="arrow-forward-outline"></ion-icon></a>
                </div>

            </div>
        </div>
    </div>
</div>

@endpush


@push('scripts')

<script>
    var data = {!! json_encode($photos)!!};
    console.log(data['Externa'])
</script>

<script>
    /* indicators-banner-carousel */
        $( document ).ready(function() {
            var currentSlider = 0;

            // carousel bg transition of desktop
            $('#carousel_todos').on('slide.bs.carousel', function (carousel) {
                $("#carousel_todos .current-carousel-pos" ).html((carousel.to + 1) + "/" ); 
                currentSlider = carousel.to;
                $(".indicators-all").each(function (event) {  
                    $(this).addClass("d-mob-none");
                })
                $("#indicators-all-"+(currentSlider - 1)).removeClass("d-mob-none")
                $("#indicators-all-"+(currentSlider + 1)).removeClass("d-mob-none")
                $("#indicators-all-"+currentSlider).removeClass("d-mob-none")

                if(currentSlider == 0){
                    $("#indicators-all-"+(currentSlider + 2)).removeClass("d-mob-none")
                }
            })
            
            $( "#fullscreenModal_todos" ).bind('click', function() {
                $("#modal_todos").modal()/* +currentSlider */
                $("#carousel_todos_full").carousel(+currentSlider)/* +currentSlider */
            });

            var currentSlider_internas = 0;

            // carousel bg transition of desktop
            $('#carousel_internas').on('slide.bs.carousel', function (carousel) {
                $("#carousel_internas .current-carousel-pos" ).html((carousel.to + 1) + "/" ); 
                currentSlider_internas = carousel.to;
                $(".indicators-intern").each(function (event) {  
                    $(this).addClass("d-mob-none");
                })
                $("#indicators-intern-"+(currentSlider - 1)).removeClass("d-mob-none")
                $("#indicators-intern-"+(currentSlider + 1)).removeClass("d-mob-none")
                $("#indicators-intern-"+currentSlider).removeClass("d-mob-none")

                if(currentSlider == 0){
                    $("#indicators-intern-"+(currentSlider + 2)).removeClass("d-mob-none")
                }

            })
            
            $( "#fullscreenModal_internas" ).bind('click', function() {
                $("#modal_internas").modal()/* +currentSlider_internas */
                $("#carousel_internas_full").carousel(+currentSlider_internas)

            });

            var currentSlider_externas = 0;

            // carousel bg transition of desktop
            $('#carousel_externas').on('slide.bs.carousel', function (carousel) {
                $("#carousel_externas .current-carousel-pos" ).html((carousel.to + 1) + "/" ); 
                currentSlider_externas = carousel.to;
                $(".indicators-extern").each(function (event) {  
                    $(this).addClass("d-mob-none");
                })
                $("#indicators-extern-"+(currentSlider - 1)).removeClass("d-mob-none")
                $("#indicators-extern-"+(currentSlider + 1)).removeClass("d-mob-none")
                $("#indicators-extern-"+currentSlider).removeClass("d-mob-none")

                if(currentSlider == 0){
                    $("#indicators-extern-"+(currentSlider + 2)).removeClass("d-mob-none")
                }

            })
            
            $( "#fullscreenModal_externas" ).bind('click', function() {
                $("#modal_externas").modal()/* +currentSlider_externas */
                $("#carousel_externas_full").carousel(+currentSlider_externas)

            });
            var currentSlider_plantas = 0;

            // carousel bg transition of desktop
            $('#carousel_plantas').on('slide.bs.carousel', function (carousel) {
                $("#carousel_plantas .current-carousel-pos" ).html((carousel.to + 1) + "/" ); 
                currentSlider_plantas = carousel.to;
                $(".indicators-plans").each(function (event) {  
                    $(this).addClass("d-mob-none");
                })
                $("#indicators-plans-"+(currentSlider - 1)).removeClass("d-mob-none")
                $("#indicators-plans-"+(currentSlider + 1)).removeClass("d-mob-none")
                $("#indicators-plans-"+currentSlider).removeClass("d-mob-none")

                if(currentSlider == 0){
                    $("#indicators-plans-"+(currentSlider + 2)).removeClass("d-mob-none")
                }

            })
            
            $( "#fullscreenModal_plantas" ).bind('click', function() {
                $("#modal_plantas").modal()/* +currentSlider_plantas */
                $("#carousel_plantas_full").carousel(+currentSlider_plantas)

            });

        });

        $( ".point-info" ).hover(
            function() {
                $(".info-hover").addClass("animated fadeIn");
            }, function() {
                $(".info-hover").removeClass("fadeIn");
            }
        );


</script>
<script>
    var data = {!! json_encode($data) !!};
    console.log("test data book");
    console.log(data);
</script>

@endpush
