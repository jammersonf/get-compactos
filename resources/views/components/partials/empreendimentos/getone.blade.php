<div class="container-fluid background-cinza conteudos-list" id="list-emp">
    <!--
    <div data-aos="fade-up" class="container d-flex justify-content-start justify-content-md-center pt-20 px-0 overflow-auto">
        <div class="list-btns d-none d-md-block">
            <a href="?q=all#list-emp" class="btn btn-dark hover-white text-uppercase @if($q == 'all') active @endif">TODOS</a>
            <a href="?q=Lançamento#list-emp" class="btn btn-dark hover-white text-uppercase @if($q == 'Lançamento') active @endif">Lançamento</a>
            <a href="?q=Em construção#list-emp" class="btn btn-dark hover-white text-uppercase @if($q == 'Em construção') active @endif">Em andamento</a>
            <a href="?q=Pronto para morar#list-emp" class="btn btn-dark hover-white text-uppercase @if($q == 'Pronto para morar') active @endif">Pronto para morar</a>
            <a href="?q=Portfólio" class="btn btn-dark hover-white text-uppercase @if($q == 'Portfólio') active @endif">Portfólio</a>
        </div>
        <div class="list-btns d-md-none d-flex btn-list-mobile">
            <a href="?q=all#list-emp" class="btn btn-dark hover-white text-uppercase @if($q == 'all') active @endif">TODOS</a>

            <a href="?q=Lançamento#list-emp" class="btn btn-dark hover-white text-uppercase @if($q == 'Lançamento') active @endif">Lançamento</a>
            <a href="?q=Em construção#list-emp" class="btn btn-dark hover-white text-uppercase @if($q == 'Em construção') active @endif">Em andamento</a>
            <a href="?q=Pronto para morar#list-emp" class="btn btn-dark hover-white text-uppercase @if($q == 'Pronto para morar') active @endif">Pronto para morar</a>
            <a href="?q=Portfólio" class="btn btn-dark hover-white text-uppercase @if($q == 'Portfólio') active @endif">Portfólio</a>
            
        </div>
    </div>
    <hr>
-->
    @forelse($emp as $data)
    <div style="padding-top: 105px" class="container d-none d-md-block">
        <div class="row m-0">
            <div data-aos="fade-right" class="col-spacing-right-sm col-md-8 d-flex justify-content-end">
                <img style="right: -10px;" class="img-bg-pattern-right" src="{{url('assets_front/imgs/img-bg-pattern.png')}}" alt="">
                
                <img onclick="location.href= '{{ route('emp', $data->slug)}}' ;" class="img-cover ap1-img click-link" src="{{url('storage/emp/'.$data->ch_img)}}" alt="">
            </div>
            <div data-aos="fade-right" data-aos-delay="200" class=" col-md-4">
                <div class="v-center col-spacing-left-sm">
                    <a href="{{ route('emp', $data->slug)}}">
                        <h1 style="max-width: 204px;word-break: break-word;" class="text-preto-azulado-get">
                            {{$data->ch_title}}
                        </h1>
                    </a>
                    <a href="{{ route('emp', $data->slug)}}">
                        <p style="max-width: 287px;">
                            {!! \Illuminate\Support\Str::limit($data->ch_subtitle, 120, '...')!!}
                        </p>
                    </a>
                    <a class="btn btn-dark-get" href="{{ route('emp', $data->slug)}}" style="white-space: nowrap;">Ver Mais</a>
                </div>
            </div>
        </div>
    </div>
    <div style="padding-top: 42px" class="container d-md-none">
        <div class="emp-mobile">
                <h1 data-aos="fade-right" style="max-width: 204px;word-break: break-word;" class="text-preto-azulado-get">
                    {{$data->ch_title}}
                </h1>
                <p data-aos="fade-right" style="max-width: 287px;">
                    {!! \Illuminate\Support\Str::limit($data->ch_subtitle, 50, '...')!!}
                </p>
            <div data-aos="fade-right" class="  d-flex justify-content-end position-relative mt-40">
                <img style="right: -10px;" class="img-bg-pattern-right-mobile" src="{{url('assets_front/imgs/img-bg-pattern.png')}}" alt="">
                
                <img  class="img-cover ap1-img click-link" src="{{url('storage/emp/'.$data->ch_img)}}" alt="">
            </div>

            <a data-aos="fade-right"  class="btn btn-dark-get px-0 w-100 mt-20" href="{{ route('emp', $data->slug)}}">Conhecer Empreendimento</a>

        </div>
    </div>
    @empty
        Sem empreendimento cadastrado
    @endforelse
</div>

@push('script')
    {{-- <script>
        var element = document.getElementById("el-link");

        var url = element.dataset.url;

        element.setAttribute('onclick', 'location.href = '+url+''); 
    </script> --}}
@endpush
