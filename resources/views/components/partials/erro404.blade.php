<div class="bg-404">
    <div class="text-center v-center">
        <h4 style="margin-bottom: 4px" class="text-white">ERRO 404</h4>
        <h2 style="margin-bottom: 10px" class="text-white">Não encontramos essa página</h2>
        <p style="max-width: 594px; margin-bottom: 28px" class="text-white mx-auto">Nos parece que esse link que você tentou acessar não existe mais. Por favor, continue navegando.</p>
        <a href="{{ route('home') }}" class="btn btn-outline-primary">Ir para Início</a>
    </div>
</div>