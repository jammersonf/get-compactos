<div id="emp-{{$emp['id']}}" class="row d-none step-1-row">
    <div class="col-md-4 text-center text-md-left mx-2 mx-md-0">
        <img src="{{url('assets_front/imgs/Blueprint.png')}}" class="pb-2" alt="">
        <h4 class="text-preto-azulado-get">
            Escolha sua unidade
        </h4>
        <p class="mob-paragraph-16 text-preto-azulado-get">
            Selecione a unidade que deseja e siga para a próxima etapa
        </p>
    </div>
    <div  class="col-md-5 pb-40 mt-40 mt-mob-20 pt-3 px-0">
        <div class="row">
            @foreach($emp['floors'] as $subkey => $flor)
            <div class="col-md-6 mx-md-0 mx-4 aways-outlined-btn-holder">
                <button onclick="resetPavmentoCollapse('arrow-toggle-{{$subkey}}', this)" style="padding: 10px 19px;"
                    class="btn btn-outline-dark btn-block mt-20 d-none d-md-block" type="button" data-toggle="collapse"
                    data-target="#col-pav-{{$subkey}}" aria-expanded="false" aria-controls="col-pav-{{$subkey}}">
                    {{$flor['name']}}
                    <ion-icon class="position-relative arrow-toggle-{{$subkey}} pl-1" style="top: 4px"
                        name="chevron-down-outline"></ion-icon>
                    <ion-icon class="position-relative arrow-toggle-{{$subkey}} d-none pl-1" style="top: 4px"
                        name="chevron-up-outline"></ion-icon>
                </button>
                <button onclick="resetPavmentoCollapse('arrow-toggle-{{$subkey}}', this)" style="padding: 10px 19px;"
                    class="btn btn-outline-dark mt-20 btn-block d-md-none" type="button" data-toggle="collapse"
                    data-target="#col-pav-{{$subkey}}" aria-expanded="false" aria-controls="col-pav-{{$subkey}}">
                    {{$flor['name']}}
                    <ion-icon class="position-relative arrow-toggle-{{$subkey}} pl-1" style="top: 4px"
                        name="chevron-down-outline"></ion-icon>
                    <ion-icon class="position-relative arrow-toggle-{{$subkey}} d-none pl-1" style="top: 4px"
                        name="chevron-up-outline"></ion-icon>
                </button>
                <div class="collapse collapse-pav-reset mx-5 mx-md-0 " id="col-pav-{{$subkey}}">
                    @foreach($flor['unities'] as $unityKey => $unity)
                    @if($unity[0]['floor_id'] == $flor['id'])
                    <a onclick="toggleUnityByMenu('#col-planta-{{$flor['id']}}-{{ $unity[0]['size_slug']}}', this, '#unity_list_{{$key}}')" class="caption-18 text-hex-797979-important d-block click-link-unities click-link" style="margin-top: 16px"
                        >Plantas
                        {{ $unity[0]['size']}}</a>
                        {{-- data-toggle="collapse" href="#col-planta-{{$flor['id']}}-{{ $unity[0]['size_slug']}}"
                        role="button" aria-expanded="false"
                        aria-controls="col-planta-{{$flor['id']}}-{{ $unity[0]['size_slug']}}" --}}
                    @endif
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div id="unity_list_{{$key}}" class="col-md-12">
        @foreach($emp['floors'] as $subkey => $flor)

        <div class="text-center planta-thumb mb-40 collapse collapse-pav-reset collapse-pav-map"
            id="col-pav-{{$subkey}}">

            <map name="map_unities_{{$flor['id']}}">
                @foreach($flor['unities'] as $unityKey => $plantas)
                @if($plantas[0]['floor_id'] == $flor['id'])

                @foreach($plantas as $singleKey => $unity)
                    <area target=""  alt="" title="" 
                        @if ($unity['sold'] == 0)
                            class="select-unidade-click" data-value="{{$unity['price']}}"
                            onclick="toggleUnityByMap(
                                                    '#col-planta-{{$flor['id']}}-{{ $plantas[0]['size_slug']}}', 
                                                    '#radio-uni-{{$flor['id']}}-{{$unity['id']}}', 
                                                    '{{url('storage/unity/'.$unity['img'])}}', 
                                                    {{json_encode($unity['planned'])}}, 
                                                    {{json_encode($flor)}}, 
                                                    {{json_encode($unity)}}
                                                    )" 
                        @endif
                        coords="{{$unity['coordinate']}}" shape="poly">
                @endforeach

                @endif
                @endforeach            
            </map>

            <p class="caption-20 text-preto-azulado-get mb-20">{{$flor['name']}}</p>
            <img usemap="#map_unities_{{$flor['id']}}" style="margin-bottom: 15px"
                class="img-fluid @if($subkey == 0) img-map-styles @endif" src="{{url('storage/floor/'.$flor['img'])}}"
                class="pb-2" alt="">
            <br>
            <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#modal-floors-{{$subkey}}">Ampliar imagem</button>
        </div>
        @endforeach
    </div>
    <div class="col-md-12">
        @foreach($emp['floors'] as $subkey => $flor)
        @foreach($flor['unities'] as $unityKey => $plantas)

        @if($plantas[0]['floor_id'] == $flor['id'])

        <div class="collapse collapse-reset" id="col-planta-{{$flor['id']}}-{{$plantas[0]['size_slug']}}">
            <div class="row">
                <div class="col-5 col-md-4 text-right">
                    <p class="caption-20 text-preto-azulado-get mb-0 mr-3">{{$plantas[0]['size']}}</p>
                    <img style="margin-bottom: 15px" class="img-fluid img-planta-uni uni-outline img-detail"
                        src="{{url('assets_front/imgs/planta-unidade.png')}}" class="pb-2" alt="">
                </div>
                <div class="col-7 pl-0 col-md-8 mt-4 mt-md-0">

                    <div class="row ">
                        @foreach($plantas as $singleKey => $unity)
                        <div class="col-md-6 mb-18 @if ($unity['sold'] != 1) get-free-select @endif">
                            <div class="custom-control custom-radio custom-control-inline get-select">
                                <input  @if ($unity['sold'] == 1) disabled @endif
                                    onclick="setUnity('{{url('storage/unity/'.$unity['img'])}}', {{json_encode($unity['planned'])}}, {{json_encode($flor)}}, {{json_encode($unity)}}, 'uni-label-{{$flor['id']}}-{{$unity['id']}}')"
                                    type="radio" id="radio-uni-{{$flor['id']}}-{{$unity['id']}}"
                                    name="radio-uni-{{$flor['id']}}-{{$unity['size']}}" data-value="{{$unity['price']}}"
                                    class="@if ($unity['sold'] == 1) get-sold-input @endif custom-control-input select-unidade" >
                                <label class="get-unity-lavel custom-control-label uni-label" id="uni-label-{{$flor['id']}}-{{$unity['id']}}"
                                    for="radio-uni-{{$flor['id']}}-{{$unity['id']}}">{{$unity['name']}} @if ($unity['sold'] == 1) <small class="text-red get-free-soldLabel">(vendido)</small> @endif</label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @endif

        @endforeach
        @endforeach

    </div>
</div>

@push('modals')
@foreach($emp['floors'] as $key => $flor)
    <div class="modal fade p-0 modal_img_emp" id="modal-floors-{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog fullscreen-modal" role="document">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">
                            <ion-icon name="close-outline"></ion-icon>
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <map name="map_unities_modal_{{$flor['id']}}">
                        @foreach($flor['unities'] as $unityKey => $plantas)
                        @if($plantas[0]['floor_id'] == $flor['id'])
        
                        @foreach($plantas as $singleKey => $unity)
                            <area target=""  alt="" title="" data-dismiss="modal" aria-label="Close"
                                @if ($unity['sold'] == 0)
                                    class="select-unidade-click" data-value="{{$unity['price']}}"
                                    onclick="toggleUnityByMap(
                                                            '#col-planta-{{$flor['id']}}-{{ $plantas[0]['size_slug']}}', 
                                                            '#radio-uni-{{$flor['id']}}-{{$unity['id']}}', 
                                                            '{{url('storage/unity/'.$unity['img'])}}', 
                                                            {{json_encode($unity['planned'])}}, 
                                                            {{json_encode($flor)}}, 
                                                            {{json_encode($unity)}}
                                                            )" 
                                @endif
                                coords="{{$unity['coordinate']}}" shape="poly">
                        @endforeach
        
                        @endif
                        @endforeach            
                    </map>
        
                    <img usemap="#map_unities_modal_{{$flor['id']}}" style="margin-bottom: 15px" class="img-fluid v-center" width="100%" height="auto" src="{{url('storage/floor/'.$flor['img'])}}" class="pb-2" alt="">
                    {{-- <img class="d-block w-100 img-cover img-fotos" style="height: 80vh"
                        src="{{url('storage/emp/'.$photo->img)}}" alt="First slide"> --}}
                </div>
            </div>
        </div>
    </div>
@endforeach
@endpush

@push('scripts')
<script>
    var previousMapUnitySelect = "";
    function toggleUnityByMap(unityGroupId, unityRadioId, setUnityPath, setUnityPlannedPath, setUnityFloor, setUnityUnity) {

        if (unityGroupId != previousMapUnitySelect){
            resetCollapse()

            previousMapUnitySelect = unityGroupId;
            $(previousMapUnitySelect).collapse('show');
        }

        $(unityRadioId).prop("checked", true);
        setUnity(setUnityPath, setUnityPlannedPath, setUnityFloor, setUnityUnity, 'uni-label-'+setUnityFloor.id+'-'+setUnityUnity.id);

    }
    function toggleUnityByMenu(unityGroupId, caller, scrolId) {
        $('html, body').animate({
            scrollTop: $(scrolId).offset().top
        }, 100);

        if (unityGroupId != previousMapUnitySelect){
            resetCollapse()

            $(".click-link-unities").each(function () {  
                $(this).removeClass("active-unity");
            })
                $(caller).addClass("active-unity");

            previousMapUnitySelect = unityGroupId;
            $(previousMapUnitySelect).collapse('show');
        }
    }
    $('.modal_img_emp').on('shown.bs.modal', function () {
        resizeMap();
    })

    $('.collapse-pav-map').each(function( index ) {
            $(this).on('shown.bs.collapse', function () {
                
                resizeMap();
                /* $('.img-map-styles').maphilight(); */

                /* $('.mapping').mouseover(function() {
                    alert($(this).attr('id'));
                }).mouseout(function(){
                    alert('Mouseout....');      
                }); */
            })
        });

        
        function resetCollapse(){
            $('.collapse-reset').each(function( index ) {
                $( this ).collapse('hide');
            });
        }
        var currentPavBtn;
        var oldBtbArrow;
        function resetPavmentoCollapse(btnArrowStr, event){
            $('.collapse-pav-reset').each(function( index ) {
                $( this ).collapse('hide');
            });
            if(oldBtbArrow == btnArrowStr){
                $('.' + btnArrowStr).each(function( index ) {
                    $( this ).toggleClass("d-none");
                });
            }else {
                $('.' + btnArrowStr).each(function( index ) {
                    $( this ).toggleClass("d-none");
                });
                $('.' + oldBtbArrow).each(function( index ) {
                    $( this ).toggleClass("d-none");
                });

            }
            oldBtbArrow = btnArrowStr;

            $( ".btn-select-pav" ).each(function( index ) {
                $( this ).removeClass("active")
            });

            if($(event).hasClass("active")){
                $(event).toggleClass("active");
            }else {
                $(currentPavBtn).removeClass("active");
                $(event).addClass("active");
            }
            currentPavBtn = event;
        }

        function setUnity(path, plannedPath, floor, unity, labelId){
            oportunity.unity.floor = floor;
            oportunity.unity.data = unity;
            

            setImg(path, plannedPath);

            $(oportunity.appliances.htmlId).addClass("d-none");
            console.log(oportunity.appliances.htmlId);
            var idString = "#collapse-electros-"+floor.id+"-"+unity.id;

            oportunity.appliances.htmlId = idString;

            $(oportunity.appliances.htmlId).removeClass("d-none");
            console.log(oportunity.appliances.htmlId);

            $(".uni-label").each(function () {  
                $(this).removeClass("active-unity");
            })
            $("#"+labelId).addClass("active-unity");
        }
        function setImg(path, plannedPath){
            setPlanned(plannedPath);
            $('.img-detail').each(function() {
                $( this ).attr("src",path);
            });
        }

        function setPlanned(obj){
            console.log(obj);
            var newHtml = "";
            var newHtmlMob = "";
            var hasPlans = false;
            obj.forEach(e => {
                console.log("plan shit");
                console.log(e);
                hasPlans = true;

                newHtml = newHtml + 
                '<div class="text-center">' +
                    `<button style="box-shadow: none;" onclick="selectPlan('${e.name}', ${e.id}, ${e.price})" class="btn plan-select-hover">` +
                        `<div style="background-image: url('{{url('storage/planned/${e.img}')}}');" class="screen-check position-relative background-check">` +
                            '<div  class="selected-img-bg-bg" ></div>' +
                            `<div id="check-selected-${e.id}" class=" op-0">` +
                                '<div  class="selected-img-bg" ></div>' +
                                '<img src="{{url('assets_front/imgs/check.svg')}}" class="pb-2 check-icon" alt="">' +
                            '</div>' +
                        '</div>' +
                        `<p class="caption-18 text-preto-azulado-get">${e.name}</p>` +
                    '</button>' +
                '</div>'
                newHtmlMob = newHtmlMob + 
                '<div class="text-center">' +
                    `<button style="box-shadow: none;" onclick="selectPlan('${e.name}', ${e.id}, ${e.price})" class="btn">` +
                        `<div style="background-image: url('{{url('storage/planned/${e.img}')}}');" class="screen-check position-relative background-check">` +
                            `<div id="check-selected-${e.id}-mob" class=" op-0">` +
                                '<div  class="selected-img-bg" ></div>' +
                                '<img src="{{url('assets_front/imgs/check.svg')}}" class="pb-2 check-icon" alt="">' +
                            '</div>' +
                        '</div>' +
                        `<p class="caption-18 text-preto-azulado-get">${e.name}</p>` +
                    '</button>' +
                '</div>'
            });
            if(hasPlans){
                $("#planejados-desc").html(
                    "Selecione o projeto de móveis planejados e siga para a próxima etapa"
                );
            }else {
                $("#planejados-desc").html(
                    "Essa unidade não possui Móveis planejados cadastrados"
                );
            }
            $(".planned-holder").html(
                newHtml
            );
            $(".planned-holder-mobile").html(
                newHtmlMob
            );
        }

</script>
@endpush