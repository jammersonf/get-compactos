
<div id="build-section" class="background-hex-E5E5E5 overflow-hidden">
    {{-- <div class="container-smaller mx-auto position-relative ">
        <div id="pageTop" class="row py-72">
            <div class="col-md-6">
                <h2 data-aos="fade-up" class="text-preto-azulado-get text-center text-md-right ">
                    Monte o seu
                </h2>
            </div>
            <div class="col-md-6">
                <p data-aos="fade-up" style="max-width: 323px;" class="paragraph-20 text-preto-azulado-get text-center text-md-left mx-auto mx-md-3 mx-md-0">
                    Escolha o empreendimento, sua unidade, móveis planejados, lista de eletrodomésticos e sugestão de pagamento. 
                </p>
            </div>
        </div>
    </div> --}}
    <div  class="container-smaller mx-auto position-relative">

        {{-- <hr style="margin-left: 80px; margin-right: 80px"> --}}

        <div style="padding-bottom: 37px; display: none !important;" class="d-flex justify-content-start justify-content-md-around ml-3 ml-md-0 step-offset over-auto-mob d-print-none">
            <div data-aos="fade-up" data-aos-delay="300" class="step-holder step-initial text-center z-1" style="max-width: 107px; min-width: 100px" >
                <div style="padding: 10px 17px"class="step mx-auto mb-2">
                    <p class="m-0">1</p>
                    <img class="check-img-step step-img-holder d-none" data-step-id="step-1" src="{{url('assets_front/imgs/step-check.svg')}}" alt="">
                </div>
                {{-- <br> --}}
                <p class="d-block">Escolha o Empreendimento</p>
                <div class="step-progress"></div>
            </div>
            <div data-aos="fade-up" data-aos-delay="300" data-step-id="step-2" class="step-holder text-center z-2 " style="max-width: 107px; min-width: 100px" >{{-- step-active --}}
                <div style="padding: 10px 17px"class="step mx-auto mb-2">
                    <p class="m-0">2</p>
                    <img class="check-img-step step-img-holder d-none" data-step-id="step-2" src="{{url('assets_front/imgs/step-check.svg')}}" alt="">
                </div>
                {{-- <br> --}}
                <p class="d-block">Escola Sua Unidade</p>
                <div class="step-progress"></div>
            </div>
            <div data-aos="fade-up" data-aos-delay="300" data-step-id="step-3" class="step-holder text-center z-3 " style="max-width: 107px; min-width: 100px" >
                <div style="padding: 10px 17px"class="step mx-auto mb-2">
                    <p class="m-0">3</p>
                    <img class="check-img-step step-img-holder d-none" data-step-id="step-3" src="{{url('assets_front/imgs/step-check.svg')}}" alt="">
                </div>
                {{-- <br> --}}
                <p class="d-block">Móveis Planejados</p>
                <div class="step-progress"></div>
            </div>
            <div data-aos="fade-up" data-aos-delay="300" data-step-id="step-4" class="step-holder text-center z-4" style="max-width: 107px; min-width: 100px" >
                <div style="padding: 10px 17px"class="step mx-auto mb-2">
                    <p class="m-0">4</p>
                    <img class="check-img-step step-img-holder d-none" data-step-id="step-4" src="{{url('assets_front/imgs/step-check.svg')}}" alt="">
                </div>
                {{-- <br> --}}
                <p class="d-block">Lista de Eletrodomésticos</p>
                <div class="step-progress"></div>
            </div>
            <div data-aos="fade-up" data-aos-delay="300" data-step-id="step-5" class="step-holder text-center z-5" style="max-width: 107px; min-width: 100px" >
                <div style="padding: 10px 17px"class="step mx-auto mb-2">
                    <p class="m-0">5</p>
                    <img class="check-img-step step-img-holder d-none" data-step-id="step-5" src="{{url('assets_front/imgs/step-check.svg')}}" alt="">
                </div>
                {{-- <br> --}}
                <p class="d-block">Sugestão de Pagamento</p>
                <div class="step-progress"></div>
            </div>
            <div data-aos="fade-up" data-aos-delay="300" data-step-id="step-6" class="step-holder text-center z-6" style="max-width: 107px; min-width: 100px" >
                <div style="padding: 10px 17px"class="step mx-auto mb-2">
                    <p class="m-0">6</p>
                    <img class="check-img-step step-img-holder d-none" data-step-id="step-6" src="{{url('assets_front/imgs/step-check.svg')}}" alt="">
                </div>
                {{-- <br> --}}
                <p class="d-block">Sua Oportunidade</p>
                <div class="step-progress"></div>
            </div>
        </div>
        {{-- desktop --}}
        <div data-aos="fade-right" id="price-tag" class="builder-value-holder d-none d-md-block animate-css">
            <img src="{{url('assets_front/imgs/retangle-get-your-price.svg')}}" alt="">
            <p class="caption-20 text-vermelho-get m-0">Get your Price</p>
            <h4 id="value-total" class="m-0">R$ 0,00</h4>
        </div>

    </div>
    <div id="stepsStart" class="container-smaller mx-auto position-relative">
        <div  id="step-1">

            @include('components.partials.monteoseu.steps.step-1')

            <div id="sec-step-1-end" data-aos="fade" class="text-center">
                <button disabled data-step-to="step-2" data-step-from="step-1" data-step-dir="next" id="disabled-emp-selec" class="btn btn-outline-dark btn-step-move btn-smaller mb-100">
                    Próximo Passo
                </button>
            </div>
        </div>
        <div  id="step-2" class="d-none">
            @foreach($data as $key => $emp)
                @include('components.partials.monteoseu.steps.step-2')
            @endforeach

            <div class="text-center mt-50">
                <button data-step-to="step-1" data-step-from="step-2" data-step-dir="prev" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Passo Anterior
                </button>
                <button disabled data-step-to="step-3" data-step-from="step-2" data-step-dir="next" id="disabled-uni-selec" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Próximo Passo
                </button>

            </div>
        </div>
        <div  id="step-3" class="d-none">
            @include('components.partials.monteoseu.steps.step-3')

            <div class="text-center">
                <button href="#pageTop" data-step-to="step-2" data-step-from="step-3" data-step-dir="prev" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Passo Anterior
                </button>
                <button href="#pageTop" data-step-to="step-4" data-step-from="step-3" data-step-dir="next" id="disabled-plan-selec" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Próximo Passo
                </button>
            </div>
        </div>
        <div  id="step-4" class="d-none">
            @include('components.partials.monteoseu.steps.step-4')

            <div class="text-center">
                <button href="#pageTop" data-step-to="step-3" data-step-from="step-4" data-step-dir="prev" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Passo Anterior
                </button>
                <button href="#pageTop" data-step-to="step-5" data-step-from="step-4" data-step-dir="next" id="disabled-forni-selec" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Próximo Passo
                </button>
            </div>
        </div>
        <div  id="step-5" class="d-none">{{--  --}}
                @include('components.partials.monteoseu.steps.step-5')
            <div class="text-center">
                <button id="sec-step-5-end"  href="#pageTop" data-step-to="step-4" data-step-from="step-5" data-step-dir="prev" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Passo Anterior
                </button>
                <button href="#pageTop" data-step-to="step-6" data-step-from="step-5" data-step-dir="next" id="disabled-forni-selec" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Próximo Passo
                </button>
            </div>
        </div>
    </div>
    <div class="container-lg mx-auto position-relative">
        <div  id="step-6" class="d-none">{{--  --}}
            @include('components.partials.monteoseu.steps.step-6')

            {{-- <div class="text-center">
                <button data-step-to="step-4" data-step-from="step-5" data-step-dir="prev" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Passo Anterior
                </button>
                <button data-step-to="step-6" data-step-from="step-5" data-step-dir="next" id="disabled-forni-selec" class="btn btn-outline-dark btn-step-move btn-smaller mb-100" >
                    Próximo Passo
                </button>
            </div> --}}
        </div>

    </div>
    
    <div data-aos="fade-right" id="price-tag-mob" class="builder-value-holder-mobile text-center d-md-none">
        <p class="caption-20 text-vermelho-get m-0">Get your Price</p>
        <h4 id="value-total-mob" class="m-0">R$ 0,00</h4>
    </div>
</div>

@push('scripts')
    <script>
        function scrollToId(id) {
            $('html, body').animate({
                scrollTop: $(id).offset().top
            }, 100);
        }
    </script>
    <script>
        var oportunity = 
        {
            is_client: false,
            development: {},
            unity: {
                price: "0"
            },
            planned_furniture: {
                price: "0",
                selected: false,
                name: "Essa unidade não possui Móveis planejados selecionados"
            },
            appliances: {
                items:[],
                price: 0
            },
            payment: {
                custom: true,
            },
            payment_sugestions: {},
            total: 0
        };

        var defaultEmp = {!! json_encode($e) !!}
        var allEmps = {!! json_encode($data) !!}
        setDefaultEmp();

        function setDefaultEmp() {
            if(defaultEmp){
                //console.log(defaultEmp);
                //console.log(allEmps);
                var defaultEmpData = allEmps.find(x => x.id == defaultEmp);
                //console.log(defaultEmpData);
                setEmpreendimento(defaultEmp, defaultEmpData, '#emp-check-'+defaultEmpData.id);
                moveStep("step-2", "step-1", "next");
            }
        }

        function setIsClient(value) { 
            if(value){
                $("#btnClient").addClass("active");
                $("#btnNotClient").removeClass("active");
                $(".get-free-select").each(function () {
                    $(this).addClass("d-none");
                });
                $(".get-sold-input").each(function () {
                    $(this).removeAttr("disabled");
                });
                /* $(".get-free-soldLabel").each(function () {
                    $(this).addClass("d-none");
                }); */
                $(".print-price-tag").each(function () {
                    $(this).addClass("d-none");
                });

            }else{
                $("#btnClient").removeClass("active");
                $("#btnNotClient").addClass("active");
                $(".get-free-select").each(function () {
                    $(this).removeClass("d-none");
                });
                /* $(".get-free-soldLabel").each(function () {
                    $(this).removeClass("d-none");
                }); */
                $(".get-sold-input").each(function () {
                    $(this).attr("disabled", true);
                    $(this).prop("checked", false);
                });
                $(".print-price-tag").each(function () {
                    $(this).removeClass("d-none");
                });


            }
            oportunity.is_client = value;
            updateValueEmpreendimento();

        }

        $( ".btn-step-move" ).click(function() {
            //console.log("this is the actual data from oportunidade");
            //console.log(oportunity);
            var from = $(this).data( "step-from" );
            var to = $(this).data( "step-to" );
            var dir = $(this).data( "step-dir" );
            moveStep(to, from, dir);
            scrollToId('#pageTop');
        });
        $('.select-unidade').each(function( index ) {
            $( this ).change(function() {
                oportunity.unity.price = $(this).data("value");
                updateValueEmpreendimento();
                $('#disabled-uni-selec').removeAttr("disabled");
            });
        });
        $('.select-unidade-click').each(function( index ) {
            $( this ).click(function() {
                oportunity.unity.price = $(this).data("value");
                updateValueEmpreendimento();
                $('#disabled-uni-selec').removeAttr("disabled");
            });
        });


        function animateCSS(element, animationName, callback) {
            const node = document.querySelector(element)
            node.classList.add('animated', animationName)

            function handleAnimationEnd() {
                node.classList.remove('animated', animationName)
                node.removeEventListener('animationend', handleAnimationEnd)

                if (typeof callback === 'function') callback()
            }

            node.addEventListener('animationend', handleAnimationEnd)
        }
        function moveStep(to, from, dir){
            console.log(dir == "prev")
            animateCSS('#'+from, 'fadeOut', function() {
                $("#"+from).removeClass("d-block");
                $("#"+from).addClass("d-none");
                $("#"+to).removeClass("d-none");
                $("#"+to).addClass("d-block");

                if(to == "step-1"){
                    hidePavmentos();
                }
                if(to == "step-2"){
                    resizeMap();
                }
                /* if(to == "step-5"){
                    console.log("goind to the step five")
                    setRangeValue();
                } */
                if(to == "step-6"){
                    $('#price-tag').addClass('op-0');
                    $('#price-tag-mob').addClass('op-0');
                    setFullEmpreendimentoPrice();
                    setFormData();
                }
                if(to == "step-5"){


                    var firstPaymentOption = oportunity.payment_sugestions[0];

                    selectPaymentSugestion(firstPaymentOption);
                }
                if(from == "step-6"){
                    
                    $('#price-tag').removeClass('op-0');
                    $('#price-tag-mob').removeClass('op-0');
                }

                animateCSS('#'+to, 'fadeIn', function() {
                })

            })

            $(".step-holder").each(function () {  
                if($(this).data("step-id") == to){
                    $(this).addClass("step-active");
                }
                if($(this).data("step-id") == from && dir == "prev"){
                    $(this).removeClass("step-active");
                }
            });
            $(".step-img-holder").each(function () {  
                if($(this).data("step-id") == from && dir == "next"){
                    $(this).removeClass("d-none");
                }
                if($(this).data("step-id") == to && dir == "prev"){
                    $(this).addClass("d-none");
                }
            });

        }

        function updateValueEmpreendimento() {
            oportunity.total = 0;
            //console.log("oportunity at start");
            //console.log(oportunity);

            if(oportunity.is_client){
                oportunity.total = +oportunity.total + +oportunity.planned_furniture.price + +oportunity.appliances.price;
            }else {
                oportunity.total = +oportunity.total + +oportunity.unity.price + +oportunity.planned_furniture.price + +oportunity.appliances.price;
            }
            //console.log("oportunity at end");
            //console.log(oportunity);
            var valueFormated = oportunity.total.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

            $('#value-total').html(valueFormated);
            $('#value-total-mob').html(valueFormated);
        }
        function setEmpreendimentoAndScroll(pavmento, emp, checkId) {
            scrollToId("#sec-step-1-end");
            setEmpreendimento(pavmento, emp, checkId);
        }
        function setEmpreendimento(pavmento, emp, checkId) {
            $(".get-check-emp").prop("checked", false);
            $(checkId).prop("checked", true);


            $('.paymentSugestionToggle').each(function( index ) {
                $(this).collapse('hide');
            });

            $('#selectPayment-'+emp.id).collapse('toggle');
            oportunity.payment_sugestions = emp.payments;
            console.log(emp);
            var data = new Date(emp.andent);
            var dataNowRaw = Date.now();
            var dataNow = new Date(dataNowRaw);
            //console.log(data);
            //console.log(dataNow);
            oportunity.development.order_day = dataNow;
            oportunity.development.deliver_day = data;

            oportunity.development.ch_title = emp.ch_title;
            oportunity.development.title = emp.title;
            oportunity.development.id = emp.id;
            
            //hide the previous html section
            $(oportunity.development.htmlId).addClass("d-none");

            oportunity.development.htmlId = '#emp-'+pavmento;
            $(oportunity.development.htmlId).removeClass("d-none");
            $('#disabled-emp-selec').removeAttr("disabled");
            console.log("disabled being take out");

            //hide the previous html section
            $(oportunity.development.checkId).addClass("op-0");
            $(oportunity.development.checkIdBg).removeClass("d-none");
            $(oportunity.development.checkIdBtn).removeClass("active");

            oportunity.development.checkId = '#check-emp-selected-'+ pavmento;
            oportunity.development.checkIdBg = '#check-emp-selected-bg-bg-'+ pavmento;
            oportunity.development.checkIdBtn = '#btn-builder-'+ pavmento;

            $(oportunity.development.checkId).removeClass("op-0");
            $(oportunity.development.checkIdBg).addClass("d-none");
            $(oportunity.development.checkIdBtn).addClass("active");

            setMinimumValue(emp.price_unity);
        }
        function resizeMap() {  
            //console.log("resized");
            $('map').imageMapResize();
            $(window).trigger('resize');
            /* $('img[usemap]').imageMap(); */
        }
        function setFullEmpreendimentoPrice (){
            var valueFormated = oportunity.total.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            var valueMovelFormatedNumber = +oportunity.planned_furniture.price;
            var valueMovelFormated = valueMovelFormatedNumber.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

            //console.log("oportunity all");
            //console.log(oportunity);

            var entradaFormated = oportunity.payment.entry.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

            var semestraisFormated = oportunity.payment.semester.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            var mensaisFormated = oportunity.payment.montly.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

            var semestraisTotalFormated = oportunity.payment.semesterTotal.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            var mensaisTotalFormated = oportunity.payment.montlyTotal.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

            var financiamentoFormated = oportunity.payment.financy.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            var unidadeFormatedNumber = +oportunity.unity.price;
            var unidadeFormated = unidadeFormatedNumber.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});


            $(' .final-value-emp').html(valueFormated);
            $(' .final-value-movel-name').html(oportunity.planned_furniture.name);
            if(valueMovelFormatedNumber == 0){
                $(' .final-value-movel-price').html("");
            }else {
                $(' .final-value-movel-price').html(valueMovelFormated);
            }

            $('#final-value-entrada').html(entradaFormated);

            $('#final-value-semestrais').html(semestraisTotalFormated);
            $('#final-value-semestrais-qtd').html(oportunity.payment.semesterTimes + "x " + semestraisFormated);

            $('#final-value-mensais').html(mensaisTotalFormated);
            $('#final-value-mensais-qtd').html(oportunity.payment.montlyTimes + "x " + mensaisFormated);

            $('#final-value-financiamento').html(financiamentoFormated);

            $('#final-value-type').html(oportunity.payment.typeTitle);
            $('#final-value-typeDesc').html(oportunity.payment.typeDesc);

            //dados da unidade selecionada
            $('#final-value-emp-name').html(oportunity.development.ch_title);
            $('#final-value-uni-name').html(oportunity.unity.data.name);
            $('#final-value-uni-price').html(unidadeFormated);

            var formatedForniList = "";

            oportunity.appliances.items.forEach(e => {
                formatedForniList = formatedForniList + `<p class=" mb-10 caption-16 text-white">${e}</p>`;
            });
            if(oportunity.appliances.items.length == 0)
                formatedForniList = '<p class=" mb-10 caption-16 text-white">Essa unidade não possui Eletrodomésticos selecionados</p>';

            $(' .forni-list').html(formatedForniList);


            //console.log("final data for display");
            //console.log(oportunity);

            /* $(' .final-moveis-emp').html(valueFormated);
            $(' .final-moveis-emp').html(valueFormated); */
        }
        function setFormData() {
            $('#get-slider-entrada').attr("value", +oportunity.payment.entry);
            //console.log(oportunity)

            $('#input-emp').attr('value',oportunity.development.id);
            $('#input-sim').attr('value',JSON.stringify(oportunity));
            $('#input-emp-mob').attr('value',oportunity.development.id);
            $('#input-sim-mob').attr('value',JSON.stringify(oportunity));
        }
        function consoleEmp(emp) {
            console.log("this is the full emp below");
            console.log(emp);
        }
        function hidePavmentos(){
            $(empreendimento).addClass("d-none");
        }
        function selectPlan(name, id, price) {  
            //planned_furniture
            console.log("select plan being called");
            console.log(name)
            console.log(id)
            console.log(price)
            if(id == oportunity.planned_furniture.id){

                oportunity.planned_furniture.price = "0";
                oportunity.planned_furniture.id = -1;
                oportunity.planned_furniture.selected = false;
                oportunity.planned_furniture.name = "Essa unidade não possui Móveis planejados selecionados";
                console.log(oportunity);
                $(oportunity.planned_furniture.htmlId).addClass('op-0');
                $(oportunity.planned_furniture.htmlIdMob).addClass('op-0');
                updateValueEmpreendimento();

                return;
            }
            oportunity.planned_furniture.id = id;
            oportunity.planned_furniture.selected = true;
            oportunity.planned_furniture.name = name;
            console.log(oportunity);

            $(oportunity.planned_furniture.htmlId).addClass('op-0');
            $(oportunity.planned_furniture.htmlIdMob).addClass('op-0');
            /* console.log(oportunity.planned_furniture.htmlId); */
            /* console.log(id);
            console.log(price); */
            oportunity.planned_furniture.htmlId = '#check-selected-'+oportunity.planned_furniture.id;
            oportunity.planned_furniture.htmlIdMob = '#check-selected-'+oportunity.planned_furniture.id+'-mob';

            $(oportunity.planned_furniture.htmlId).removeClass('op-0');
            $(oportunity.planned_furniture.htmlIdMob).removeClass('op-0');
            /* console.log(oportunity.planned_furniture.htmlId); */


            oportunity.planned_furniture.price = ""+price;

            updateValueEmpreendimento();
            //$('#disabled-plan-selec').removeAttr("disabled");

        }
        
        function setMinimumValue (value) { 
            oportunity.total = +value;
            oportunity.unity.price = +value;
            var price = +value;
            var valueFormated = price.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            if(!oportunity.is_client){
                $('#value-total').html(valueFormated);
                $('#value-total-mob').html(valueFormated);
            }
        }
        function updateEntradaValue(val){
            var trueValue = +val;
            var valueFormated = trueValue.toLocaleString();

            $('#payment-entrada').html(valueFormated);
        }
        function updateSemestraisValue(val){
            var trueValue = +val;
            var valueFormated = trueValue.toLocaleString();

            $('#payment-semestrais').html(valueFormated);
        }
        function updateMensaisValue(val){
            var trueValue = +val;
            var valueFormated = trueValue.toLocaleString();

            $('#payment-mensais').html(valueFormated);
        }
        function updateFinanciamentoValue(val){
            var trueValue = +val;
            var valueFormated = trueValue.toLocaleString();

            $('#payment-financiamento').html(valueFormated);
        }


        function setMovel(price, name, el) {

            if(el.checked){
                oportunity.appliances.items.push(name);
                oportunity.appliances.itemsObj.push({"name": name, "price": price});
                oportunity.appliances.price =  oportunity.appliances.price + price;
                updateValueEmpreendimento();
            }else{
                const index = oportunity.appliances.items.indexOf(name);
                if (index > -1) {
                    oportunity.appliances.items.splice(index, 1);
                    oportunity.appliances.itemsObj.splice(index, 1);
                }


                oportunity.appliances.price =  oportunity.appliances.price - price;
                updateValueEmpreendimento();
            }
            $('#disabled-forni-selec').removeAttr("disabled");
        }

        var setPriceVisible = function () {
            var o1 = $("#menu").offset();
            var o2 = $("body").offset();
            var dx = o1.left - o2.left;
            var dy = o1.top - o2.top;
            var distance = Math.sqrt(dx * dx + dy * dy);

            if (distance > 200) {

                $("#price-tag").removeClass("right-offset");
                
            } else {

                $("#price-tag").addClass("right-offset");



            }
        }

        setPriceVisible();

        $(window).scroll(function (event) {
            setPriceVisible();

        });

    </script>
@endpush