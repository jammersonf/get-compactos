<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="theme-color" content="#2c4e87">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />

  @include('admin.partials.styles')
  @include('admin.partials.favicons')
  @toastr_css
</head>
<body>
  <div id="app">
    <div class="main-wrapper">
      @include('admin.partials.nav')
      @include('admin.partials.sidebar')
      <div class="main-content">
        @yield('content')
      </div>
      @include('admin.partials.footer')
    </div>
    @include('admin.partials.scripts')
    @toastr_js
    @toastr_render
    @yield('scripts')
  </div>
</body>
</html>
