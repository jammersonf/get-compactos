@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Dashboard</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-4 col-lg-4">
        <div class="card dashboard-funil">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Últimos 1
            </h4>
          </div>
          <div class="card-body -scroll">
            <ul class="list-unstyled list-unstyled-border">
            </ul>
          </div>
          <div class="card-footer text-center">
            <a href="" class="text-muted">Ver todos</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-4 col-lg-4">
        <div class="card dashboard-funil">
          <div class="card-header ">{{-- -success --}}
            <h4>
              <i class="far fa-star lga"></i> Últimos 2
            </h4>
          </div>
          <div class="card-body -scroll">
            <ul class="list-unstyled list-unstyled-border">
            </ul>
          </div>
          <div class="card-footer text-center">
            <a href="" class="text-muted">Ver todos</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-4 col-lg-4">
        <div class="card dashboard-funil">
          <div class="card-header ">{{-- -warning --}}
            <h4>
              <i class="far fa-envelope lga"></i> Últimas Mensagens
            </h4>
          </div>
          <div class="card-body -scroll">
            <ul class="list-unstyled list-unstyled-border">

            </ul>
          </div>
          <div class="card-footer text-center">
            <a href="" class="text-muted">Ver todos</a>
          </div>
        </div>
      </div>
    </div>
     <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header -info">
            <h4>
              <i class="fas fa-rocket lga"></i> Acesso Rápido
            </h4>
          </div>
          <div class="card-body">
            <div class="row">
                <!--
                <div class="col-lg-3">
                  <div class="statistics">
                    <a href="" class="hoverAll"></a>
                    <div class="score-blue">Adicionar</div>
                    <header class="text-center">
                      <h6>#</h6>
                    </header>
                  </div>
                </div>
              -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
