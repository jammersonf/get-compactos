<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('type', 'Status', ['class' => '']) !!}
    {!! Form::select('type', [
      'Interna' => 'Interna',
      'Externa' => 'Externa',
      'Planta' => 'Planta',
    ],old('type'), ['class' => 'form-control', 'requred']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-2">
  <div class="form-group mb-2">
    {!! Form::label('order', 'Ordem', ['class' => '']) !!}<br>
    {!! Form::number('order', old('order'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img', 'Imagem', ['class' => '']) !!}<br>
    @if(isset($data) && $data->ch_img) <img src="{{url('storage/emp/'.$data->ch_img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img', old('img'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('1tt', 'Superior 1', ['class' => '']) !!}
    {!! Form::text('1tt', old('1tt'), ['class' => 'form-control']) !!}
    @if($errors->has('1tt'))
      <span class="text-danger">{{ $errors->first('1tt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('1stt', 'Legenda', ['class' => '']) !!}
    {!! Form::text('1stt', old('1stt'), ['class' => 'form-control']) !!}
    @if($errors->has('1stt'))
      <span class="text-danger">{{ $errors->first('1stt') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('2tt', 'Superior 2', ['class' => '']) !!}
    {!! Form::text('2tt', old('2tt'), ['class' => 'form-control']) !!}
    @if($errors->has('2tt'))
      <span class="text-danger">{{ $errors->first('2tt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-22 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('2stt', 'Legenda', ['class' => '']) !!}
    {!! Form::text('2stt', old('2stt'), ['class' => 'form-control']) !!}
    @if($errors->has('2stt'))
      <span class="text-danger">{{ $errors->first('2stt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('3tt', 'Superior 3', ['class' => '']) !!}
    {!! Form::text('3tt', old('3tt'), ['class' => 'form-control']) !!}
    @if($errors->has('3tt'))
      <span class="text-danger">{{ $errors->first('3tt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-32 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('3stt', 'Legenda', ['class' => '']) !!}
    {!! Form::text('3stt', old('3stt'), ['class' => 'form-control']) !!}
    @if($errors->has('3stt'))
      <span class="text-danger">{{ $errors->first('3stt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('4tt', 'Inferior 1', ['class' => '']) !!}
    {!! Form::text('4tt', old('4tt'), ['class' => 'form-control']) !!}
    @if($errors->has('4tt'))
      <span class="text-danger">{{ $errors->first('4tt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-42 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('4stt', 'Legenda', ['class' => '']) !!}
    {!! Form::text('4stt', old('4stt'), ['class' => 'form-control']) !!}
    @if($errors->has('4stt'))
      <span class="text-danger">{{ $errors->first('4stt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('5tt', 'Inferior 2', ['class' => '']) !!}
    {!! Form::text('5tt', old('5tt'), ['class' => 'form-control']) !!}
    @if($errors->has('5tt'))
      <span class="text-danger">{{ $errors->first('5tt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-52 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('5stt', 'Legenda', ['class' => '']) !!}
    {!! Form::text('5stt', old('5stt'), ['class' => 'form-control']) !!}
    @if($errors->has('5stt'))
      <span class="text-danger">{{ $errors->first('5stt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('6tt', 'Inferior 3', ['class' => '']) !!}
    {!! Form::text('6tt', old('6tt'), ['class' => 'form-control']) !!}
    @if($errors->has('6tt'))
      <span class="text-danger">{{ $errors->first('6tt') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-62 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('6stt', 'Legenda', ['class' => '']) !!}
    {!! Form::text('6stt', old('6stt'), ['class' => 'form-control']) !!}
    @if($errors->has('6stt'))
      <span class="text-danger">{{ $errors->first('6stt') }}</span>
    @endif
  </div>
</div>

