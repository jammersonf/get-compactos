@php 
  $img = isset($data) ? [] : ['required']; 
  $img2 = isset($data) ? [] : ['required']; 
@endphp
<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('name', 'Name', ['class' => '']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control', ' required']) !!}
    @if($errors->has('name'))
      <span class="text-danger">{{ $errors->first('name') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-2">
  <div class="form-group mb-2">
    {!! Form::label('size', 'Tamanho', ['class' => '']) !!}
    {!! Form::text('size', old('size'), ['class' => 'form-control', ' required']) !!}
    @if($errors->has('size'))
      <span class="text-danger">{{ $errors->first('size') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('img', 'Imagem', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img) <img src="{{url('storage/unity/'.$data->img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img', $img) !!}
  </div>
</div>

<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('img2', 'Img/tamanho', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img2) <img src="{{url('storage/unity/'.$data->img2)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img2', $img2) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('coordinate', 'Coordenadas', ['class' => '']) !!}
    {!! Form::text('coordinate', old('coordinate'), ['class' => 'form-control', ' required']) !!}
    @if($errors->has('coordinate'))
      <span class="text-danger">{{ $errors->first('coordinate') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('price', 'Valor', ['class' => '']) !!}
    {!! Form::text('price', old('price'), ['class' => 'form-control', 'id' => 'currency', 'required']) !!}
    @if($errors->has('price'))
    <span class="text-danger">{{ $errors->first('price') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-2">
  <div class="form-group mb-2">
    {!! Form::label('sold', 'Vendido', ['class' => '']) !!}<br>
    {!! Form::checkbox('sold', old('sold')) !!}
  </div>
</div>
<div class="col-sm-12 col-md-12">
  <h2>Eletrodomésticos</h2>
</div>
@foreach($electros as $e)
  <div class="col-sm-12 col-md-3">
    <div class="form-group mb-3">
      {!! Form::checkbox('electros[]', $e) !!} {{$e->name}}
    </div>
  </div>
@endforeach
<div class="col-sm-12 col-md-12"></div>
@section('scripts')
  @include('admin.partials._maskmoney')
@endsection
