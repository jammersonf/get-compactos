<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpFloor extends Model
{
    protected $table = 'emp_floor';
    
    protected $fillable = [
        'emp_id',
        'name',
        'img'
    ];

    public function unities()
    {
        return $this->hasMany(EmpFloorUnity::class, 'floor_id');
    }

    public function getUnityGroupedByFloor($id)
    {
        $unities = EmpFloorUnity::orderBy('name', 'ASC')
                                ->where('floor_id', $id)
                                ->with('planned')
                                ->get()
                                ->groupBy('size')
                                ->toArray();
        
        return $unities;
    }
}
