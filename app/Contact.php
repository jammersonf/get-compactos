<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Contact extends Model
{
    protected $table = 'contact';
    
    protected $fillable = [
        'name', 'mail', 'phone', 'msg', 'emp_id', 'type'
    ];

    public function emp()
    {
        return $this->belongsTo(Emp::class, 'emp_id');
    }

    public function simulation()
    {
        return $this->hasOne(ContactSimulation::class, 'contact_id');
    }

    public function getTypeTxtAttribute()
    {
        return Str::limit($this->type, 4, '');
    }
}
