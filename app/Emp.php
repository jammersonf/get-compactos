<?php

namespace App;

use App\EmpFloorUnity;
use Illuminate\Database\Eloquent\Model;

class Emp extends Model
{
    protected $table = 'emp';

    protected $appends = ['price_unity'];
    
    protected $fillable = [
        'ch_img',
        'ch_title',
        'ch_subtitle',
        'background',
        'logo',
        'head_title',
        'head_subtitle',
        'title',
        'slug',
        'txt',
        'status',
        'img1',
        'img2',
        'column1',
        'column2',
        'icon1',
        'txt1',
        'icon2',
        'txt2',
        'icon3',
        'txt3',
        'icon4',
        'txt4',
        'icon5',
        'txt5',
        'icon6',
        'txt6',
        'icon7',
        'txt7',
        'icon8',
        'txt8',
        'icon9',
        'txt9',
        'icon10',
        'txt10',
        'icon11',
        'txt11',
        'icon12',
        'txt12',
        'icon13',
        'txt13',
        'icon14',
        'txt14',
        'icon15',
        'txt15',
        'book',
        'imgbook',
        'andtxt1',
        'andpct1',
        'andtxt2',
        'andpct2',
        'andtxt3',
        'andpct3',
        'andtxt4',
        'andpct4',
        'andtxt5',
        'andpct5',
        'andtxt6',
        'andpct6',
        'andtxt7',
        'andpct7',
        'andent',
        'andpr',
        'lat',
        'long',
        'imgPin',
        'price'
    ];

    public function contact()
    {
        return $this->hasMany(Contact::class, 'emp_id');
    }

    public function floors()
    {
        return $this->hasMany(EmpFloor::class, 'emp_id');
    }

    public function payments()
    {
        return $this->hasMany(EmpPayment::class, 'emp_id');
    }

    public function photos()
    {
        return $this->hasMany(EmpPhoto::class, 'emp_id');
    }

    public function getBuilder()
    {
        $emp = $this->with(['floors', 'payments'])->get()->toArray();
        $data = [];
        foreach($emp as $e) {
            $data[] = $e;
        }
        foreach($data as $emp => $d){
            foreach($d['floors'] as $key => $floor) {
                $unities = (new EmpFloor)->getUnityGroupedByFloor($floor['id']);
                $data[$emp]['floors'][$key]['unities'] = $unities;
            }
        }

        return $data;
    }

    public function getPriceUnityAttribute()
    {
        $unity = EmpFloorUnity::where('emp_id', $this->id)
                            ->orderBy('price', 'ASC')
                            ->first();
        return $unity ? $unity->price : 0;
    }
}
