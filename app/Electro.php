<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Electro extends Model
{
    protected $table = 'electros';
    
    protected $fillable = [
        'name',
        'desc',
        'price'
    ];
}
