<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home2 extends Model
{
    protected $table = 'home2';

    protected $fillable = [
        'title',
        'subtitle',
        'img1',
        'img2'
    ];
}
