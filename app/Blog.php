<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';
    
    protected $fillable = [
        'title', 'slug', 'subtitle', 'name', 'txt', 'img', 'views'
    ];

    public function getDateAttribute()
    {
        $date = $this->created_at;
        $dt = strtoupper($date->locale('pt_BR')->monthName);
        return $date->format('d').' '.$dt.', '.$date->format('Y');
    }
}
