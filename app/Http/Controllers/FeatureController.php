<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Home1;
use Illuminate\Http\Request;

class FeatureController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Home1::paginate();

        return view('admin.feature.index', compact('data'));
    }

    public function create()
    {        
        return view('admin.feature.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $imageName = time().'0.'.$input['img1']->extension();
        $input['img1']->move(public_path('storage/feat'), $imageName);

        $input['img1'] = $imageName;

        $imageName = time().'1.'.$input['img2']->extension();
        $input['img2']->move(public_path('storage/feat'), $imageName);

        $input['img2'] = $imageName;

        $imageName = time().'2.'.$input['img3']->extension();
        $input['img3']->move(public_path('storage/feat'), $imageName);

        $input['img3'] = $imageName;
        
        Home1::create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.feat.index');
    }

    public function show($id)
    {
        $data = Home1::find($id);

        return view('admin.feature.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img1'])) {
            $imageName = time().'0.'.$input['img1']->extension();
            $input['img1']->move(public_path('storage/feat'), $imageName);

            $input['img1'] = $imageName;
        } else {
            unset($input['img1']);
        }

        if (isset($input['img2'])) {
            $imageName = time().'1.'.$input['img2']->extension();
            $input['img2']->move(public_path('storage/feat'), $imageName);

            $input['img2'] = $imageName;
        } else {
            unset($input['img2']);
        }

        if (isset($input['img3'])) {
            $imageName = time().'2.'.$input['img3']->extension();
            $input['img3']->move(public_path('storage/feat'), $imageName);

            $input['img3'] = $imageName;
        } else {
            unset($input['img3']);
        }
        
        Home1::find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.feat.index');
    }

    public function destroy($id)
    {
        Home1::find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.feat.index');
    }
}
