<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\AdminContact;
use App\Mail\MailContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except(['index', 'store']);
    }

    public function index(Request $request)
    {
        return view('pages.contact');
    }

    public function store(Request $request)
    {
        $input = $request->only(['name', 'mail', 'phone', 'msg', 'emp_id']);
        $input['type'] = $this->setType($request);
        $message = Contact::create($input);
        if ($input['type']  == 'simulação') {
            $simulation = $this->setSimulation(json_decode($request->payment_sim, true));
            
            $message->simulation()->create($simulation);
        }
        
        Mail::to('diretoria@construtoratropical.com')->send(new AdminContact($message));
        Mail::to('marketing@construtoratropical.com')->send(new AdminContact($message));
        Mail::to('comercial@construtoratropical.com')->send(new AdminContact($message));
        Mail::to($input['mail'])->send(new MailContact($message));

        toastr()->success('Contato Enviado!');

        return redirect()->route('home');
    }

    public function simulation($id)
    {
        $data = Contact::with(['simulation', 'emp'])->find($id);

        return view('admin.contact.simulation', compact('data'));
    }

    public function setSimulation($data)
    {
        unset($data['unity']['floor']['unities']);
        unset($data['unity']['data']['electros']);
        unset($data['unity']['data']['planned']);

        $arr = [
            'emp_id'  => $data['development']['id'],
            'floor'   => $data['unity']['floor'],
            'unity'   => $data['unity']['data'],
            'project' => $data['planned_furniture'],
            'electro' => $data['appliances'],
            'payment' => $data['payment'],
            'total'   => $data['total']
        ];

        return $arr;
    }

    public function setType($request)
    {
        if ($request->has('payment_sim')) {
            $type = 'Simulação no Monte o Seu';
        } else if($request->has('emp_id')) {
            $type = 'Empreendimento';
        } else {
            $type = 'Contato';
        }

        return $type;
    }

    public function admin()
    {
        $data = Contact::with('emp')->orderBy('id', 'desc')->paginate();

        return view('admin.contact.index', compact('data'));
    }
}
