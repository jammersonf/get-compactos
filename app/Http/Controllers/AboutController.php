<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AboutController extends Controller
{
    protected $about;

    public function __construct(About $about) {
        $this->middleware('auth')->except(['index']);
        $this->about = $about;
    }

    public function index()
    {
        $data = $this->about->all();
        
        return view('pages.about', compact('data'));
    }

    public function admin()
    {
        $data = $this->about->paginate();

        return view('admin.about.index', compact('data'));
    }

    public function create()
    {        
        return view('admin.about.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        if (!isset($input['title'])) {
            $input['title'] = '';
        }
        if (isset($input['img1'])) {
            $imageName = time().'.'.$input['img1']->extension();
            $input['img1']->move(public_path('storage/about'), $imageName);
            $input['img1'] = $imageName;
        } else {
            $input['img1'] = ''; 
        }

        if (isset($input['img2'])) {
            $imageName = time().'2.'.$input['img2']->extension();
            $input['img2']->move(public_path('storage/about'), $imageName);

            $input['img2'] = $imageName;
        }
        
        $this->about->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.about.admin');
    }

    public function show($id)
    {
        $data = $this->about->find($id);

        return view('admin.about.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img1'])) {
            $imageName = time().'1.'.$input['img1']->extension();
            $input['img1']->move(public_path('storage/about'), $imageName);
            $input['img1'] = $imageName;
        } else {
            unset($input['img1']);
        }

        if (isset($input['img2'])) {
            $imageName = time().'2.'.$input['img2']->extension();
            $input['img2']->move(public_path('storage/about'), $imageName);
            $input['img2'] = $imageName;
        } else {
            unset($input['img2']);
        }
        
        $this->about->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.about.admin');
    }

    public function destroy($id)
    {
        $this->about->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.about.admin');
    }
}
