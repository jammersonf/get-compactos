<?php

namespace App\Http\Controllers;

use App\About;
use App\Emp;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BuilderController extends Controller
{
    public function index(Request $request)
    {
        $data = (new Emp)->getBuilder();
        $e = isset($request->e) ? $request->e : null;
        
        return view('pages.builder', compact('data', 'e'));
    }
}
