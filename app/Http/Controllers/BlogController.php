<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    protected $blog;

    public function __construct(Blog $blog) {
        $this->middleware('auth');
        $this->blog = $blog;
    }

    public function index()
    {
        $data = $this->blog->paginate();

        return view('admin.blog.index', compact('data'));
    }

    public function create()
    {        
        return view('admin.blog.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['name'] = '';
        $input['slug'] = Str::slug($input['title'], '-');
        $imageName = time().'.'.$input['img']->extension();
        $input['img']->move(public_path('storage/blog'), $imageName);
        $input['img'] = $imageName;
        
        $this->blog->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.blog.index');
    }

    public function show($id)
    {
        $data = $this->blog->find($id);

        return view('admin.blog.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $imageName = time().'.'.$input['img']->extension();
            $input['img']->move(public_path('storage/blog'), $imageName);
            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }
        
        $this->blog->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.blog.index');
    }

    public function destroy($id)
    {
        $this->blog->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.blog.index');
    }
}
