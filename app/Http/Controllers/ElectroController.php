<?php

namespace App\Http\Controllers;

use App\Electro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ElectroController extends Controller
{
    protected $electro;

    public function __construct(Electro $electro) {
        $this->middleware('auth');
        $this->electro = $electro;
    }

    public function index()
    {
        $data = $this->electro->paginate();

        return view('admin.electro.index', compact('data'));
    }

    public function create()
    {        
        return view('admin.electro.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['price'] = $this->toUsd($input['price']);
        $this->electro->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.electro.index');
    }

    public function show($id)
    {
        $data = $this->electro->find($id);

        return view('admin.electro.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $input['price'] = $this->toUsd($input['price']);
        
        $this->electro->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.electro.index');
    }

    public function destroy($id)
    {
        $this->electro->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.electro.index');
    }
}
