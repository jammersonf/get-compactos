<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PinCat extends Model
{
    protected $table = 'pin_cat';
    
    protected $fillable = [
        'cat',
        'img',
        'color',
        'slug'
    ];

    public function addresses()
    {
        return $this->hasMany(PinAd::class, 'cat_id');
    }
}
